<?php

require '../modelos/Manescuela.php';
require '../modelos/escuela.php';
if (isset($_GET["opcion"])) {
    $opciones = $_GET["opcion"];
    if ($opciones == "L") {
        $data = array();
        $output = array();
        $ManteEscuela = ModeloMantEscuela();
        foreach ($ManteEscuela as $ManteEscuela) {
            $output["id_escuela_sabatica"] = $ManteEscuela["id_escuela_sabatica"];
            $output["escuela"] = $ManteEscuela["escuela"];
            $output["id_unidadacad_univ"] = $ManteEscuela["id_unidadacad_univ"];
            $output["id_univ_unidacad"] = $ManteEscuela["id_univ_unidacad"];
            $output["facultad"] = $ManteEscuela["facultad"];
            $output["abreviatura"] = $ManteEscuela["abreviatura"];
            $output["boton"] = '<center><button type="button" class="btn btn-outline-success waves-effect btn-sm" id"UpdEsM" onclick="EditarManEscuela(\'' . $ManteEscuela["id_escuela_sabatica"] . '\',\'' . $ManteEscuela["escuela"] . '\',\'' . $ManteEscuela["id_univ_unidacad"] . '\')" data-toggle="modal" data-target="#ModalUdpEsc"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button><button class="btn btn-outline-danger waves-effect btn-sm" id"agregargale" onclick="DelManEscuela(\'' . $ManteEscuela["id_escuela_sabatica"] . '\')"><i class="fa fa-trash" aria-hidden="true"></i></button></center>';
            $data[] = $output;
        }
        $salida = array('data' => $data);
        echo json_encode($salida);
    }
      if ($opciones == "RepUs") {
        $data = array();
        $output = array();
        $ManteEscuela = ModeloRepUser();
        foreach ($ManteEscuela as $ManteEscuela) {
            $output["login"] = $ManteEscuela["login"];
            $output["persona"] = $ManteEscuela["persona"];
            $output["rol"] = $ManteEscuela["rol"];
            $data[] = $output;
        }
        $salida = array('data' => $data);
        echo json_encode($salida);
    }
    
    if ($opciones == "UDP") {
        $DatosControl = array(
            "nombre" => $_POST["manescue"],
            "id_unidadacad_univ" => (int) $_POST["facultadup"],
            "id_escuela_sabatica" => (int) $_POST["d"]);
        $repsu = EditarDatosEscuela($DatosControl);

        $output["alert"] = $repsu["alert"];
        $output["mensaje"] = $repsu["mensaje"];
        $data[] = $output;
        echo json_encode($data);
    }
    if ($opciones == "DEL") {
        
        $repsus = DelDatosEscuela($_POST["id"]);
        $output["alert"] = $repsus["alert"];
        $output["mensaje"] = $repsus["mensaje"];
        $data[] = $output;
        echo json_encode($data);
    }
    if ($opciones == "ADD") {
          $DatosControl = array(
            "nombre" => $_POST["escuela"],
            "id_unidadacad_univ" => (int) $_POST["id_facultad"]
            );
        $repsusss = AddDatosEscuela($DatosControl);

        $output["alert"] = $repsusss["alert"];
        $output["mensaje"] = $repsusss["mensaje"];
        $data[] = $output;
        echo json_encode($data);
    }
}