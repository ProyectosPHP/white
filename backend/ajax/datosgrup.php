<?php

require_once '../modelos/GpPequeModeloAgr.php';
require_once '../modelos/GpPequemodelos.php';
require_once '../modelos/DEL.php';
if (isset($_GET["opcion"])) {
    $opcion = $_GET["opcion"];
    if ($opcion == "ADDDAGRUP") {
        $datoosaddgrupp = array(
            "idgruper" => $_POST["idgruper"],
            "idpersona" => $_POST["idpersona"]
        );
        $validar = AgregaoGrupopersona($datoosaddgrupp);
        if ($validar == "success") {
            echo "OK";
        } else {
            echo "NO";
        }
    }
    if ($opcion == "UDPCA") {
        $datoosaddgrupp = array(
            "id_integrante_cargo" => $_POST["inteCargo"],
            "id_cargo" => $_POST["id_cargo"],
            "id_registro" => $_POST["registro"]
        );
        $validarCargo = ModeloValidarCargogrupo($_POST["usuario"], $_POST["id_cargo"]);
        if ($validarCargo["nro"] == "" && $validarCargo["id_argo"] == "" || $validarCargo["id_argo"] == "7") {
            $validar = ModelActualizarCargo($datoosaddgrupp);
            $output["alert"] = $validar["alert"];
            $output["mensaje"] = $validar["mensaje"];
            $data[] = $output;
            echo json_encode($data);
        } else {
            $output["alert"] = "warning";
            $output["mensaje"] = "EL CARGO YA ESTA OCUPADO POR FAVOR SELECCIONE OTRO";
            $data[] = $output;
            echo json_encode($data);
        }
    }
    if ($opcion == "DEL") {
        $DelInteg = MODELODELInteg($_POST["cargo"], $_POST["registro"]);
        $output["alert"] = $DelInteg["alert"];
        $output["mensaje"] = $DelInteg["mensaje"];
        $data[] = $output;
        echo json_encode($data);
    }
    
     if ($opcion == "VE") {
        $VaE=MODELOValidaEvaluar();
        $output["id"] = $VaE["id"];
        $output["alert"] = $VaE["alert"];
        $output["mensaje"] = $VaE["mensaje"];
        $data[] = $output;
        echo json_encode($data);
    }
}    