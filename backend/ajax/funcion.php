<?php
function upload_image_Alumno() {
    if (isset($_FILES["stude_img"])) {
        $extension = explode('.', $_FILES['stude_img']['name']);
        $new_name = rand() . '.' . $extension[1];
        $destination = '../vista/recursos/persona/' . $new_name;
        move_uploaded_file($_FILES['stude_img']['tmp_name'], $destination);
        return $new_name;
    }
}
function upload_image() {
    if (isset($_FILES["fotousuario"])) {
        $extension = explode('.', $_FILES['fotousuario']['name']);
        $new_name = rand() . '.' . $extension[1];
        $destination = '../vista/images/usuario/' . $new_name;
        move_uploaded_file($_FILES['fotousuario']['tmp_name'], $destination);
        return $new_name;
    }else{
        $new_name="user.svg";
        return $new_name;
    }
}

function validarespuesta($ControlDatos){
    
                $respuestas = ModeloAgregarUsuario($ControlDatos);

                $output["alert"] = $respuestas["alert_usu"];
                $output["mensaje"] = $respuestas["mensaje_usu"];
                $data[] = $output;
                echo json_encode($data);
}

function carpeta_image_articulo($image) {
    unlink("../vista/images/articulo/" . $image);
}
function carpeta_image_usuario($image) {
    unlink("../vista/images/usuario/" . $image);
}

function get_image_usuario($user_id) {
  
    $stmt = Conexion::conectar()->prepare("SELECT foto FROM usuario WHERE  id=$user_id");
    $stmt->execute();
    $result=$stmt->fetchAll();
   
    foreach ($result as $row) {
        return $row["foto"];
    }
}


function get_image_articulo($user_id) {
  
    $stmt = Conexion::conectar()->prepare("SELECT fotoprinci FROM articulos WHERE idarticulo=$user_id");
    $stmt->execute();
    $result=$stmt->fetchAll();
   
    foreach ($result as $row) {
        return $row["fotoprinci"];
    }
}

function upload_image_user() {
    if (isset($_FILES["user_image"])) {
        $extension = explode('.', $_FILES['user_image']['name']);
        $new_name = rand() . '.' . $extension[1];
        $destination = '../vista/images/usuario/' . $new_name;
        move_uploaded_file($_FILES['fotousuario']['tmp_name'], $destination);
        return $new_name;
    }
}
function upload_image_galeria() {
    if (isset($_FILES["foto"])) {
        $extension = explode('.', $_FILES['foto']['name']);
        $new_name = rand() . '.' . $extension[1];
        $destination = '../vista/images/galeria/' . $new_name;
        move_uploaded_file($_FILES['foto']['tmp_name'], $destination);
        return $new_name;
    }
}