
<?php

require '../modelos/GpPequemodelos.php';
require '../modelos/GpPequeModeloAgr.php';
require_once '../modelos/evaluar.php';
$grupo = ModeloGrupoPequeno();
$grupoperiodo = ModeloGrupoPeriodo();
$añoss = date("Y");


if (isset($_GET["opcion"])) {

    if ($_GET["opcion"] == "SemanaADDD") {
        $data = array();
        $output = array();
        $datosmodel = array("id_periodo" => $_POST["periodo"],
            "nombre" => $_POST["SEMANA"],
            "descripcion" => $_POST["LECCION"],
            "fecha_ini" => $_POST["fecha_ini"],
            "fecha_fin" => $_POST["fecha_fin"]);

        $ingresardatos = AgregarSemana($datosmodel);
     if ($ingresardatos == "success") {
            $output["Estado"] = "OK";
            $output["mensaje"] = "LA SEMANA SE REGISTRO CORRECTAMENTE";
            $output["alert"] = "success";
        } else {
            $output["Estado"] = "NO";
            $output["mensaje"] = "ERROR AL INGREGRESAR LA SEMANA";
            $output["alert"] = "error";
        }


        $data[] = $output;


        $salida = array('data' => $data);
        echo json_encode($salida);
    }



    if ($_GET["opcion"] == "SemanaAdd") {
        $data = array();
        $output = array();

        $validarSemana = ValidarIngresoSemana($_POST["periodo"], $_POST["fecha_ini"], $_POST["fecha_fin"]);
        if ($validarSemana["va"] == 0) {
            $output["Estado"] = "OK";
            $output["mensaje"] = "LAS FECHAS YA ESTAN REGISTRADOS";
            $output["alert"] = "warning";
        } else {
            $output["Estado"] = "NO";
            $output["mensaje"] = "LAS FECHAS YA ESTAN REGISTRADOS";
            $output["alert"] = "warning";
        }


        $data[] = $output;


        $salida = array('data' => $data);
        echo json_encode($salida);
    }






    if ($_GET["opcion"] == "MostraPeriAdd") {
        $data = array();
        $output = array();

        $AñoPeriodo = ModeloAñoPeriodo();
        foreach ($AñoPeriodo as $AñoPeriodo) {
            $output["id_periodo"] = $AñoPeriodo["id_periodo"];
            $output["nombre"] = $AñoPeriodo["nombre"];
            $output["A"] = $_POST["anio"];
            if ($AñoPeriodo["nombre"] >= $añoss) {
                if ($AñoPeriodo["nombre"] == $_POST["anio"]) {
                    $output["C"] = '<option value="' . $AñoPeriodo["id_periodo"] . '" selected="">' . $AñoPeriodo["nombre"] . '</option>';
                } else {
                    $output["C"] = '<option value="' . $AñoPeriodo["id_periodo"] . '">' . $AñoPeriodo["nombre"] . '</option>';
                }
            }


            $output["estado"] = $AñoPeriodo["estado"];
            $data[] = $output;
        }

        $salida = array('data' => $data);
        echo json_encode($salida);
    }



    if ($_GET["opcion"] == "Semana") {
        $data = array();
        $output = array();
        date_default_timezone_set('Europe/Madrid');
// En windows
        setlocale(LC_TIME, 'spanish');
        $AñoPeriodos = ModeloTablSEMANA($_POST["id"]);
        foreach ($AñoPeriodos as $AñoPeriodos) {
            $output["id_semana"] = $AñoPeriodos["id_semana"];
            $output["anio"] = $AñoPeriodos["anio"];
            $output["semana"] = $AñoPeriodos["semana"];
            $output["leccion"] = $AñoPeriodos["leccion"];
            $output["fecha_ini"] = $AñoPeriodos["fecha_ini"];
            $output["fecha_fin"] = $AñoPeriodos["fecha_fin"];
            $anioinicio = date_create($AñoPeriodos["fecha_ini"]);
            $aniofin = date_create($AñoPeriodos["fecha_fin"]);

            $output["fecha_ini_form"] = strftime("%d de %B del %Y", strtotime(date_format($anioinicio, 'd-m-Y')));
            $output["fecha_fin_form"] = strftime("%d de %B del %Y", strtotime(date_format($aniofin, 'd-m-Y')));
            if ($AñoPeriodos["anio"] == $añoss) {
                $output["boton"] = '<center><div class="btn-group">
                        <button class="btn btn-default" type="button"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-default" type="button"><i class="fa fa-trash"></i></button>
                      </div></center>';
            } else {
                $output["boton"] = '<center><div class="btn-group">
                        <button class="btn btn-default" type="button"><i class="fa fa-trash"></i></button>
                      </div></center>';
            }


            $data[] = $output;
        }
        $salida = array('data' => $data);
        echo json_encode($salida);
    }


    if ($_GET["opcion"] == "añosPeriodos") {
        $data = array();
        $output = array();

        $AñoPeriodo = ModeloAñoPeriodo();
        foreach ($AñoPeriodo as $AñoPeriodo) {
            $output["id_periodo"] = $AñoPeriodo["id_periodo"];
            $output["nombre"] = $AñoPeriodo["nombre"];
            $output["A"] = $_POST["anio"];
            if ($AñoPeriodo["nombre"] == $_POST["anio"]) {
                $output["C"] = '<option value="' . $AñoPeriodo["nombre"] . '" selected="">' . $AñoPeriodo["nombre"] . '</option>';
            } else {
                $output["C"] = '<option value="' . $AñoPeriodo["nombre"] . '">' . $AñoPeriodo["nombre"] . '</option>';
            }

            $output["estado"] = $AñoPeriodo["estado"];
            $data[] = $output;
        }

        $salida = array('data' => $data);
        echo json_encode($salida);
    }
    if ($_GET["opcion"] == "gp") {
        foreach ($grupo as $grupo) {
            $areglos["data"][] = $grupo;
        }
        echo json_encode($areglos);
    }
    if ($_GET["opcion"] == "DatosEscuel") {
        $escuelasaba = ModeloAdminEsccid($_POST["idescuela"]);
        foreach ($escuelasaba as $escuelasaba) {
            $areglos[] = $escuelasaba;
        }
        echo json_encode($areglos);
    }
    if ($_GET["opcion"] == "BCGru") {
        $idescuedatosgru = ModeloAdminEscID($_POST["is_escuela"]);
        $data = array();
        $output = array();
        foreach ($idescuedatosgru as $idescuedatosgru) {
            $output["data"][] = $idescuedatosgru;
        }

        echo json_encode($output);
    }
    if ($_GET["opcion"] == "grupope") {
        $data = array();
        $output = array();
        $grupopeque = ModeloGrupoPeque($_POST["id"]);
        foreach ($grupopeque as $grupopeque) {
            $output["id"] = $grupopeque["id_grupo_periodo"];
            $output["nombre"] = $grupopeque["nombre"];
            $output["boton"] = '<center><a class="btn btn-outline-primary waves-effect btn-sm" id"agregargale" onclick="agregar(\'' . $grupopeque["id_grupo_periodo"] . '\')"><i class="fa fa-plus-square" aria-hidden="true"></i> AGREGAR</a></center>';
            $data[] = $output;
        }
        $salida = array('data' => $data);
        echo json_encode($salida);
    }
    if ($_GET["opcion"] == "gpe") {
        foreach ($grupoperiodo as $grupoperiodo) {
            $areglo["data"][] = $grupoperiodo;
        }
        echo json_encode($areglo);
    }
    if ($_GET["opcion"] == "con") {
        $id_escuela = $_POST["id"];
        $data = array();
        $output = array();
        if ($id_escuela == 0) {
            $adminse = ModeloAdminEsc();
            foreach ($adminse as $adminse) {
                $output["nombregp"] = $adminse["grupo_pequeno"];
                $output["escuela"] = $adminse["escuela_sabatica"];
                $output["carrera"] = $adminse["carrera_gp"];
                $output["lider"] = $adminse["lider"];
                $data[] = $output;
            }
            $salida = array('data' => $data);
            echo json_encode($salida);
        } else {
            $adminse = ModeloAdminEscID($id_escuela);
            if (count($adminse) != 0) {
                foreach ($adminse as $adminse) {
                    $output["nombregp"] = $adminse["grupo_pequeno"];
                    $output["escuela"] = $adminse["escuela_sabatica"];
                    $output["carrera"] = $adminse["carrera_gp"];
                    $output["lider"] = $adminse["lider"];
                    $data[] = $output;
                }
                $salida = array('data' => $data);
                echo json_encode($salida);
            } else {
                $output["nombregp"] = "NO";
                $output["escuela"] = "HAY";
                $output["carrera"] = "DATOS";
                $output["lider"] = "EN LA TABLA";

                $data[] = $output;

                $salida = array('data' => $data);
                echo json_encode($salida);
            }
        }
    }
    if ($_GET["opcion"] == "edge") {
        $datogrups = Modeloidgrupperio($_POST["id"]);
        foreach ($datogrups as $datogrups) {
            $aregl["data"][] = $datogrups;
        }
        echo json_encode($aregl);
    }
    if ($_GET["opcion"] == "tabeva") {
        $data = array();
        $output = array();
        $Datoevaluar = ModeloDatoevaluarId($_POST["id"]);

        if ($_POST["id"] == 0) {

            $output["id"] = "NO HAY DATOS";
            $output["nombre"] = "NO HAY DATOS";
            $output["id_registro"] = "NO HAY DATOS";
            $output["boton"] = "NO HAY DATOS";
            $data[] = $output;
        } else {
            foreach ($Datoevaluar as $Datoevaluar) {
                $output["id"] = $Datoevaluar["id_integrante"];
                $output["nombre"] = $Datoevaluar["integrante"];
                $output["id_registro"] = $Datoevaluar["id_registro"];
                $output["boton"] = '<center><button type="button" class="btn-floating   waves-effect ValidarEbalu" title="EVALUAR" data-toggle="modal" data-numero="' . $Datoevaluar["id_registro"] . '" style="background:#0275d8; " id="evaluar" ><i class="fa fa-star-half-o" aria-hidden="true" ></i> EVALUAR</button></center>';
                $data[] = $output;
            }
        }

        $salida = array('data' => $data);
        echo json_encode($salida);
    }
    if ($_GET["opcion"] == "perso") {
        $personas = ModelotablaPersona($_POST["id"]);
        foreach ($personas as $personas) {
            $aregl["data"][] = $personas;
        }
        echo json_encode($aregl);
    }
    if ($_GET["opcion"] == "cofiglid") {
        $data = array();
        $output = array();
        $Configlider = Configlider($_POST["id"]);
        foreach ($Configlider as $Configlider) {
            $output["id_registro"] = $Configlider["id_registro"];
            $output["id_integrante"] = $Configlider["id_integrante"];
            $output["integrante"] = $Configlider["integrante"];
            $output["id_integrante_cargo"] = $Configlider["id_integrante_cargo"];
            $output["id_cargo_gp"] = $Configlider["id_cargo_gp"];
            $output["cargo"] = $Configlider["cargo"];
            $data[] = $output;
        }
        $salida = array('data' => $data);
        echo json_encode($salida);
    }

    if ($_GET["opcion"] == "personaiglesia") {
        $antes = ModeloidPersonav2antes($_POST["idpersona"], $_POST["query"]);
        $despues = ModeloidPersonav2next($_POST["idpersona"], $_POST["query"]);
        $personaid = ModeloidPersonav2($_POST["idpersona"]);
        $evaluaritem = Modeloevaluaritem($_POST["idiglesia"]);
        $if_previous_disable = '';
        $if_next_disable = '';
        if ($antes["id_persona"] == "") {
            $if_previous_disable = 'disabled="disabled"';
        }
        if ($despues["id_persona"] == "") {
            $if_next_disable = 'disabled="disabled"';
        }
        echo '<div class="modal-title w-100" id="myModalLabel">
                        <center>
                            <button type="button" class="btn-floating btn-sm blue previous" id="' . $antes["id_persona"] . '"  ' . $if_previous_disable . '  >
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn-floating btn-sm blue next" id="' . $despues["id_persona"] . '" ' . $if_next_disable . '>
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>
                        </center> 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -35px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <!--Body-->
                <div class="modal-body" >';
        echo '<div class="row"><center><h2 class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"  id="nombreevalu" style="margin-bottom: 30px;">' . $personaid["nombres"] . ' ' . $personaid["apepat"] . ' ' . $personaid["apemat"] . '</h2></center>';
        echo '<input type="hidden" id="id_estudiantes" value="' . $_POST["idregistro"] . '"/>';
        echo '<input type="hidden" id="id_estudiante" value="' . $_POST["idpersona"] . '"/>';
        $x = 0;
        foreach ($evaluaritem as $evaluaritem) {
            $x++;
            echo '<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">';
            if ($evaluaritem["desripcion"] == "number") {
                echo '<div class="form-group"><input type="' . $evaluaritem["desripcion"] . '" class="filled-in iteme check" data-numero="' . $evaluaritem["id_evaluacion_config"] . '" id="valordias" name="valordias"  value="0"  placeholder="' . $evaluaritem["nombre"] . '"></div>'
                . ' <label>' . $evaluaritem["nombre"] . '</label>';
            }
            if ($evaluaritem["desripcion"] == "checkbox") {
                echo '<div class="form-group">
                                    <input type="' . $evaluaritem["desripcion"] . '" class="filled-in iteme check" data-numero="' . $evaluaritem["id_evaluacion_config"] . '" value="SI"  id="checkbox' . $x . '">
                                    <label for="' . $evaluaritem["desripcion"] . $x . '">' . $evaluaritem["nombre"] . '</label>
                                </div>';
            }

            echo '</div>';
        }
        echo '</div>';
    }

    if ($_GET["opcion"] == "dg") {

        $datogrup = ModeloDatogrup($_POST["id"]);

        echo ' <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> ESCUELA SABATICA:' . strtoupper($datogrup["escuela"]) . '</li>
                                <li><i class="fa fa-users" aria-hidden="true"></i> GRUPO PEQUEÑO: ' . strtoupper($datogrup["nombregp"]) . '</li>
                                    <div style="hidden" id="idgru" data-numero="' . $datogrup["id_grupo_periodo"] . '"></div>
                                <li class="m-top-xs">
                                  <i class="fa fa-expeditedssl" aria-hidden="true"></i>
                                    LIDER: ' . strtoupper($datogrup["lider"]) . '
                                </li>
                                <li><i class="fa fa-book " aria-hidden="true"></i> LEMA: ';
        if ($datogrup["lema"] != null) {
            echo $datogrup["lema"];
        } else {
            echo 'NO HAY DATOS';
        }
        echo '
                                </li>

                                <li>
                                   <i class="fa fa-play-circle" aria-hidden="true"></i> CANTO: ';
        if ($datogrup["canto"] != null) {
            echo $datogrup["canto"];
        } else {
            echo 'NO HAY DATOS';
        }
        echo '</li>

                                <li class="m-top-xs">
                                    <i class="fa fa-bookmark " aria-hidden="true"></i>
                                     VERSICULO: ';
        if ($datogrup["versiculo"] != null) {
            echo $datogrup["versiculo"];
        } else {
            echo 'NO HAY DATOS';
        }
        echo '</li>
                                <li>
                                 <i class="fa fa-sort-numeric-desc " aria-hidden="true"></i> BLANCO BAUTISMO: ';
        if ($datogrup["blanco_baut"] != null) {
            echo $datogrup["blanco_baut"];
        } else {
            echo 'NO HAY DATOS';
        }
        echo '</li>';

        ;
    }


    if ($_GET["opcion"] == "evaluar") {
        $id = $_POST["id"];

        $idpersonaiglesia = ModeloidPersonaiglesia($id);
        $datogrup = ModeloDatogrup($id);
        $Validardatogrup = ValidarModeloDatogrup($id);
        $cantidaddatosgrup = $Validardatogrup["valor"];

        if ($cantidaddatosgrup > "0") {

            echo ' <li><i class="fa fa-graduation-cap" aria-hidden="true"></i>' . $cantidaddatosgrup . ' ESCUELA SABATICA:' . strtoupper($datogrup["escuela"]) . '</li>
               <li><i class="fa fa-users" aria-hidden="true"></i> GRUPO PEQUEÑO: ' . strtoupper($datogrup["nombregp"]) . '</li>
               <div style="hidden" id="idgru" data-numero="' . $datogrup["id_grupo_periodo"] . '"></div>
               <div style="hidden" id="idiglesiaperso" data-numero="' . $idpersonaiglesia["id_iglesia"] . '"></div>
               <li><i class="fa fa-book " aria-hidden="true"></i> LEMA: ';
            if ($datogrup["lema"] != null) {
                echo $datogrup["lema"];
            } else {
                echo 'NO HAY DATOS';
            }
            echo '</li><li><i class="fa fa-play-circle" aria-hidden="true"></i> CANTO: ';
            if ($datogrup["canto"] != null) {
                echo $datogrup["canto"];
            } else {
                echo 'NO HAY DATOS';
            }
            echo '</li>

                                <li class="m-top-xs">
                                    <i class="fa fa-bookmark " aria-hidden="true"></i>
                                     VERSICULO: ';
            if ($datogrup["versiculo"] != null) {
                echo $datogrup["versiculo"];
            } else {
                echo 'NO HAY DATOS';
            }
            echo '</li>
                                <li>
                                 <i class="fa fa-sort-numeric-desc " aria-hidden="true"></i> BLANCO BAUTISMO: ';
            if ($datogrup["blanco_baut"] != null) {
                echo $datogrup["blanco_baut"];
            } else {
                echo 'NO HAY DATOS';
            }
            echo '</li>';
        } else {

            echo ' <li><i class="fa fa-graduation-cap" aria-hidden="true"></i> ESCUELA SABATICA:<strong class"text-danger">TIENE QUE SER LIDER!!</strong> </li>
               <li><i class="fa fa-users" aria-hidden="true"></i> GRUPO PEQUEÑO:<strong class"text-danger">TIENE QUE SER LIDER!!</strong> </li>
               <li><i class="fa fa-book " aria-hidden="true"></i> LEMA:<strong class"text-danger">TIENE QUE SER LIDER!!</strong></li>
               <li><i class="fa fa-play-circle" aria-hidden="true"></i> CANTO:<strong class"text-danger">TIENE QUE SER LIDER!!</strong> </li>
               <li class="m-top-xs"><i class="fa fa-bookmark " aria-hidden="true"></i>VERSICULO:<strong class"text-danger">TIENE QUE SER LIDER!!</strong> </li>
               <li><i class="fa fa-sort-numeric-desc " aria-hidden="true"></i> BLANCO BAUTISMO:<strong class"text-danger">TIENE QUE SER LIDER!!</strong> </li>';
        }
    }
}
?>

