<?php
require_once 'modelos/RolModelo.php';
require_once 'modelos/Modulosmodelo.php';
?> 
<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target, // newly activated tab
                e.relatedTarget;// previous active tab
    });
</script>
<div class="card text-center" style="margin-top: 15px; background: #FFFFFF;">
    <div class="card-header default-color-dark white-text" style="padding: 5px;">
        <center><h3>PERMISOS</h3></center>
    </div>
    <div class="card-block">
        <div class="table-responsive">

            <?php
            $rol = ModeloRol();
            foreach ($rol as $rol) {
                ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="margin-top: 10px;">
                    <div class="card card-cascade narrower">
                        <script>
                            $(function () {
                                $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
                            });</script>
                        <!--Card image-->
                        <div class="view overlay hm-white-slight mdb-lightbox">
                            <img src="vista/recursos/images/superuser_logo.png" class="img-fluid " alt="">
                            <a>
                                <div class="mask waves-effect waves-light"></div>
                            </a>
                        </div>
                        <div class="card-block wow fadeInUp">
                            <h5 class="red-text"><i class="fa fa-money"></i> <?php echo $rol["nombre"] ?></h5>
                            <!--Title-->
                            <?php
                            $modul = ModeloPermisos($rol["id_rol"]);
                            foreach ($modul as $modul) {
                                ?>

                                <div class="text-left " style=" padding-left: 8px;">

                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-bottom: 15px;">
                                            <b style="margin-left: 10px; font-size: 14px;"><?php echo $modul["descripcion"] ?></b><!-- Switch -->        
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="nav navbar-right" style="margin-right: 25px; ">
                                                <div class="switch " >
                                                    <label>
                                                        OFF
                                                        <?php if ($modul["estado"] == 'true') { ?>
                                                            <input id="permiso" data-numero="<?php echo $modul["id_rol_opcion"] ?>" checked="" value="true" type="checkbox">
                                                        <?php } else { ?>
                                                            <input id="permiso" data-numero="<?php echo $modul["id_rol_opcion"] ?>"  value="false" type="checkbox">        
                                                        <?php } ?>
                                                        <span class="lever"></span>
                                                        ON
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php } ?> 
        </div>
    </div>
    <div class="card-footer text-muted default-color-dark white-text">
    </div>
</div>
