<?php
require_once 'modelos/evaluar.php';
require_once 'modelos/GpPequemodelos.php';

$lideress=ModeloDatogrup($_SESSION["usuario"]["id_usuario"]);
$validarlider="";
        if(isset($lideress["id_grupo_periodo"])){
          $validarlider=$lideress["id_grupo_periodo"] ;}
            else { 
          $validarlider=0; 
                
            } ;
?>


<script>
    $(document).ready(function () {
        evagrup(<?php echo $_SESSION["usuario"]["id_usuario"]; ?>);
      tableevaluar(<?php echo $validarlider;?>);  
    });</script>
<div class="" style="margin-top: -35px; background: #FFFFFF;">
    <div class=" primary-color-dark white-text" style="padding: 5px;"><center><h3><b>AGREGAR INTEGRANTES</b></h3></center></div>
    <div class="" style="background: #FFFFFF;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" id="nose" data-dato="<?php echo $validarlider; ?>">

                    <div class="x_content " >
                        <div class="card col-lg-3 col-md-3 col-sm-3 col-xs-12 profile_left" style="padding: 10px;" >
                            <ul id="datogrup" class="list-unstyled user_data" style="margin-top: 15px;"></ul>
                        </div>
                        <div class="table-responsive col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <table id="tableevaluar"  class="table hover table-striped table-bordered " cellspacing="0" width="100%" >
                                <thead>
                                    <tr >
                                        <th class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">  INTEGRANTES </th>
                                        <th class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">EVALUAR</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin de modal de agregar escuela sabatica-->
<form id="seleccionaperso" name="seleccionaperso">
    <div class="modal fade " id="sepersona" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg " role="document" >
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100" id="myModalLabel"><center><b>AGREGAR INTEGRANTES</b></center></h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="text-right col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#basicExample">
                                    <i class="fa fa-edit m-right-xs"></i>  AGREGAR PERSONA
                                </button>


                            </div>
                        </div>

                        <div class="table-responsive col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <table id="tablesepersona" class="table hover table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" style="margin-top: 15px;">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;" class="col-lg-1">FOTO</th>
                                        <th>NOMBRES</th>
                                        <th class="col-lg-4">ACCIONES</th>

                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <CENTER>
                        <button type="submit" class="btn btn-primary">REGISTRAR</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                    </CENTER>
                </div>

            </div>
            <!--/.Content-->
        </div>
    </div>
</form>


<!-- Modal -->
<div class="modal fade" id="cargo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-success" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel"><center>CARGO</center></h4>
            </div>
            <!--Body-->
            <div class="modal-body">

            </div>
            <!--Footer-->
            <div class="modal-footer">
                <center><button type="button" class="btn btn-primary">GUARDAR</button></center>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->



<!-- Modal -->
<div class="modal fade" id="basicExample" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel"><center>AGREGAR PERSONA</center></h4>
            </div>
            <!--Body-->
            <div class="modal-body">



            </div>
            <!--Footer-->
            <div class="modal-footer">
                <CENTER>
                    <button type="submit" class="btn btn-primary">REGISTRAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                </CENTER>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->

<!--/.Panel-->
<form id="editardatogrupe" name="editardatogrupe">
    <div class="modal fade modicaldatgru" id="modicadatgrupe" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg " role="document" >
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100" id="myModalLabel"><center><b>REGISTRAR GRUPO PEQUEÑO</b></center></h4>
                </div>
                <!--Body-->
                <div class="modal-body">


                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <CENTER>
                        <button type="submit" class="btn btn-primary">REGISTRAR</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                    </CENTER>
                </div>

            </div>
            <!--/.Content-->
        </div>
    </div>
</form>

<script type="text/javascript">

    $(".chosen").chosen({
        width: "100%",

        no_results_text: "NO HAY RESULTADO"
    });
</script>


<style>
    @media screen and (max-width: 602px) and (min-width: 0px) {
        #nombreevalu{

        }
    }
</style>

<!-- Modal -->
<div class="modal fade" id="modalevaluar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <form id="evaluarform">
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header" id="mostrarpersonaitemevalu">
                    

                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> GUARDAR</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> CERRAR</button>
                    </center>
                </div>
            </div>
        </form>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->


