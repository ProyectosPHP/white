<?php
require_once 'modelos/GpPequemodelos.php';
require_once 'modales.php';
$rol = ModeloAdminRol();
$tipodoc= ModeloTipodoc();
$carreras= ModeloCarrera();
$ciclos= ModeloCiclo();
?>
<style>
    /*    @media screen and (max-width: 699px) and (min-width: 992px) {
            div#sectionciclo{
                margin-top: 100px;
            }
        }*/
    @media screen and (min-width: 992px) {
        select#sectionciclo{
            margin-top: 100px;
        }
    }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>2017-II</h2>
                <div class="nav navbar-right ">
                    <h2>IGLESIA / VILLA UNION</h2>   
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <center><h3>REGISTRO USUARIO</h3></center>
                <br>
                <form  method="POST"  id="foraddusuario" action="ajax/prueba.php" enctype="multipart/form-data" >
                    <div class="row">
                        <input type="hidden" id="opcion" name="opcion" class="form-control" value="1">
                        <input type="hidden" id="caso" name="caso" class="form-control" >
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                            <select id="adminrol" adminrol name="adminrol" class="mdb-select colorful-select dropdown-default">
                                <?php foreach ($rol as $rol) { ?>
                                    <option value="<?php echo $rol["id_rol"]; ?>" style="font-size: 5px;"><?php echo $rol["nombre"]; ?></option>
                                <?php } ?>
                            </select>
                            <label>SELECCIONA ROL</label>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 " id="tcodigo" >
                            <div class="md-form">
                                <input type="number" id="codigo" name="codigo" maxlength="9" class="form-control" placeholder="INGRESAR CODIGO">
                                <label for="CODIGO" class="">CODIGO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tnomgrupe">
                            <div class="md-form">
                                <input type="text" id="nomgrupe" name="nomgrupe" class="form-control" placeholder="INGRESAR NOMBRE">
                                <label for="GRUPO" class="">NOMBRE:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tapepat">
                            <div class="md-form">
                                <input type="text" id="apepat" name="apepat" class="form-control" placeholder="INGRESAR APELLIDO PATERNO">
                                <label for="GRUPO" class="">APELLIDO PATERNO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tapemat">
                            <div class="md-form">
                                <input type="text" id="apemat" name="apemat" class="form-control" placeholder="INGRESAR APELLIDO MATERNO">
                                <label for="GRUPO" class="">APELLIDO MATERNO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4" id="ttipodocument">
                            <label for="IGLESIA" class="">TIPO DOCUMENTO:</label>
                            <div class="md-form">
                                <select id="tipodocument" name="tipodocument" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR TIPO DOCUMENTO</option>
                                    <?php
                                    foreach ($tipodoc as $tipodoc) {
                                        ?>
                                        <option value="<?php echo $tipodoc["id_tipo_doc"]; ?>"><?php echo $tipodoc["abreviatura"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tnrodoc">
                            <div class="md-form">
                                <input type="number" id="nrodoc" name="nrodoc" maxlength="8" class="form-control" placeholder="INGRESAR NUMERO DE DOCUMENTO">
                                <label for="GRUPO" class="">NUMERO DE DOCUMENTO:</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4" id="tcarrera">
                            <label for="IGLESIA" class="">CARRERA:</label>
                            <div class="md-form">
                                <select id="carrera" name="carrera" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR CARRERA</option>
                                    <?php
                                    foreach ($carreras as $carreras) {
                                        ?>
                                        <option value="<?php echo $carreras["id_carrera"]; ?>"><?php echo $carreras["abreviatura"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4" id="tciclo">
                            <label for="IGLESIA" class="">CICLO:</label>
                            <div class="md-form">
                                <select id="ciclo" name="ciclo" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR CICLO</option>
                                    <?php
                                    foreach ($ciclos as $ciclos) {
                                        ?>
                                        <option value="<?php echo $ciclos["id_ciclo"]; ?>"><?php echo $ciclos["nro_romanos"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tfechanac">
                            <label for="GRUPO" class="">FECHA DE NACIMIENTO:</label>
                            <div class="md-form" style="margin-top: -25px;">
                                <input type="date" id="fechanac" name="fechanac" class="form-control" placeholder="INGRESAR FECHA DE NACIMIENTO">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="ttelefono">
                            <div class="md-form">
                                <input type="number" maxlength="9" id="telefono" name="telefono" class="form-control" placeholder="INGRESAR TELEFONO">
                                <label for="GRUPO" class="">TELEFONO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tcorreo">
                            <div class="md-form">
                                <input type="text" id="correo" name="correo" class="form-control" placeholder="INGRESAR CORREO">
                                <label for="GRUPO" class="">CORREO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tdireccion">
                            <div class="md-form">
                                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="INGRESAR DIRECCION">
                                <label for="GRUPO" class="">DIRECCION:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                            <label for="GRUPO" class="">FOTO:</label>
                            <div class="md-form">

                                <input type="file" id="fotousuario" name="fotousuario" >

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                            <div id="gender" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-cyan" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="genero" id="genero" required="" value="M" data-parsley-multiple="gender" data-parsley-id="12">MASCULINO
                                </label>
                                <label class="btn btn-pink " data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="genero" required="" id="genero" value="F" data-parsley-multiple="gender">FEMENINO
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tusuario">
                            <div class="md-form">
                                <input type="text" id="usuario" name="usuario" required="" class="form-control" placeholder="INGRESAR USUARIO">
                                <label for="GRUPO" class="">USUARIO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tclave">
                            <div class="md-form">
                                <input type="password" id="clave" name="clave" required="" class="form-control" placeholder="INGRESAR CLAVE">
                                <label for="GRUPO" class="">clave:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="tbtnRegistrar">
                            <center>
                                <button type="submit" class="btn btn-outline-primary waves-effect">REGISTRAR</button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> <script type="text/javascript">
        $(document).ready(function () {
            $('.mdb-select').material_select();
        });
        $(".chosen").chosen({
            width: "100%",

            no_results_text: "NO HAY RESULTADO"
        });
    </script>