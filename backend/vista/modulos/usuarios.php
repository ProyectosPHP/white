<?php
require_once 'modelos/GpPequemodelos.php';
$mostraFacultad= ModeloCarreraFacultad();
//$AddmostraFacultad= ModeloCarreraFacultad();
?>
<div class="card" style="margin-top: 15px; background: #FFFFFF;">
    <div class="card-header primary-color-dark white-text" style="padding: 5px;"><center><h3><b>MANTENIMIENTO DE USUARIOS</b></h3></center></div>
    <div class="card-block" style="background: #FFFFFF;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form>
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="text-right">
                                    <a class="btn btn-primary waves-effect waves-light"  data-toggle="modal" data-target="#ModalADDEscueS" > <i class="fa fa-plus-circle" aria-hidden="true"></i> AGREGAR ES</a>
                                </div>
                            </div>
                            
                        </div>
                        </form>
                        <div class="col-sm-12 table-responsive">
                            <center>
                            <table id="TbUsuarios" class="table hover table-striped table-bordered nowrap " cellspacing="0" width="100%" style="margin-top: 15px;">
                                <thead>
                                    <tr>
                                        <th>USUARIO</th>
                                        <th>NOMBRES</th>
                                        <th>ACCIONES</th>                                        
                                    </tr>
                                </thead>
                            </table>
                            </center>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/.Panel-->
<script type="text/javascript">
    $(".chosen").chosen({
        width: "100%",

        no_results_text: "NO HAY RESULTADO"
    });
</script>

<!-- Button trigger modal -->


<!-- Modal -->
<form id="FormUdpEsc">
<div class="modal fade" id="ModalUdpEsc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel"><center><strong>EDITAR ESCUELA SABATICA</strong></center></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="md-form">
                            <input type="hidden" id="d" name="d" class="form-control" placeholder="INGRESAR ESCUELA">
                                <input type="text" id="manescue" name="manescue" class="form-control" placeholder="INGRESAR ESCUELA">
                                <label for="GRUPO" class="">ESCUELA:</label>
                            </div>
                    </div>
                    
                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                             <label>SELECCIONA FACULTAD</label>
                            <select id="facultadup"  name="facultadup" class="form-control colorful-select dropdown-default">
                                <?php foreach ($mostraFacultad as $mostraFacultad) { ?>
                                    <option value="<?php echo $mostraFacultad["id_univ_unidacad"]; ?>"><?php echo $mostraFacultad["abreviatura"]; ?></option>
                                <?php } ?>
                            </select>
                            
                        </div>
                    
                    
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <center>
                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                </center>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->

             </form>   


<!-- Modal -->
<form id="FormAddEscssss" method="POST" action="ajax/prueba.php">
<div class="modal fade" id="ModalADDEscueS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">AGREGAR ESCUELA SABATICA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body">
              <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="md-form">
                            
                                <input type="text" id="escuela" name="escuela" class="form-control" placeholder="INGRESAR ESCUELA">
                                <label for="GRUPO" class="">ESCUELA:</label>
                            </div>
                    </div>
                    
                         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                             <label>SELECCIONA FACULTAD</label>
                            <select id="id_facultad"  name="id_facultad" class="form-control colorful-select dropdown-default">
                                <?php foreach ($AddmostraFacultad as $AddmostraFacultad) { ?>
                                    <option value="<?php echo $AddmostraFacultad["id_univ_unidacad"]; ?>"><?php echo $AddmostraFacultad["abreviatura"]; ?></option>
                                <?php } ?>
                            </select>
                            
                        </div>
                    
                    
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                
                <button type="submit" class="btn btn-primary">GUARDAR</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->

</form>