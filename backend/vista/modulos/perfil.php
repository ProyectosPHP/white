<?php
require_once 'modelos/GpPequemodelos.php';
require_once 'modales.php';
$rol = ModeloAdminRol();

?>
<style>
    /*    @media screen and (max-width: 699px) and (min-width: 992px) {
            div#sectionciclo{
                margin-top: 100px;
            }
        }*/
    @media screen and (min-width: 992px) {
        select#sectionciclo{
            margin-top: 100px;
        }
    }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_content">
                <center><h3>AGREGAR PEFIL</h3></center>
                <br>
                <form  method="POST" action="controlador/ControlGrupoPeriodo.php?opcion=add" id="foraddgruperi">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <select id="padminrol"  name="padminrol" class="mdb-select colorful-select dropdown-default">
                                    <?php foreach ($rol as $rol) { ?>
                                    <option value="<?php echo $rol["id_rol"]; ?>" <?php if($rol["id_rol"]==1){?>selected=""<?php } ?> style="font-size: 5px;"><?php echo $rol["nombre"]; ?></option>
                                    <?php } ?>
                                </select>
                                <label>SELECCIONA ROL</label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="mostrarmodulosperfil" style="height: 350px; border: solid 2px #003bb3; overflow-y: scroll;">
                        
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 table-responsive">
                            <table id="pefilmodulo" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>NOMBRE</th>
                                        <th style="width: 50px;">ACCIONES</th>
                                    </tr>
                                </thead>
                                
                            </table>

                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div> 
    <script type="text/javascript">
        $(document).ready(function () {
            $('.mdb-select').material_select();
        });
        $(".chosen").chosen({
            width: "100%",

            no_results_text: "NO HAY RESULTADO"
        });
    </script>