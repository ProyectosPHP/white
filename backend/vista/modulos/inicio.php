<!--Section: Blog v.4-->
<section class="section section-blog-fw">

    <!--First row-->
    <div class="row">
        <div class="col-md-12">
            <!--Featured image-->
            <div class="view overlay hm-white-slight">
                <img src="vista/img/LogoGP_1.jpg" class="img-responsive img-fluid" style=" width: 100%;">
                <a>
                    <div class=""></div>
                </a>
            </div>

            <!--Post data-->
            <div class="jumbotron">
                <h2><a>BIENVENIDO AL SISTEMA DE ADMINISTRACION DE ESCUELA SABATICA</a></h2>
                <p>UNIVERSIDAD PERUANA UNIÓN<a>-TARAPOTO</a>,2017</p>

                <!--Social shares-->


            </div>
            <!--/Post data-->

            <!--Excerpt-->
            <div class="excerpt">
                <div class="row">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" >
                        <div class="tile-stats" >
                            <div class="icon" style="margin-top: 15px;"><i class="fa fa-caret-square-o-right"></i>
                            </div>
                            <div class="count">179</div>

                            <h3>GP.PEQUENO</h3>
                            <p>Lorem ipsum psdea itgum rixt.</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon" style="margin-top: 15px;"><i class="fa fa-comments-o"></i>
                            </div>
                            <div class="count">179</div>

                            <h3>ESC.SABATICA</h3>
                            <p>Lorem ipsum psdea itgum rixt.</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" >
                            <div class="icon" style="margin-top: 15px;"><i class="fa fa-sort-amount-desc"></i>
                            </div>
                            <div class="count">179</div>

                            <h3>INTEGRANTES</h3>
                            <p>Lorem ipsum psdea itgum rixt.</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats" >
                            <div class="icon" style="margin-top: 15px;"><i class="fa fa-check-square-o"></i>
                            </div>
                            <div class="count">179</div>

                            <h3>IGLESIA</h3>
                            <p>Lorem ipsum psdea itgum rixt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/First row-->

</section>
<!--/Section: Blog v.4-->