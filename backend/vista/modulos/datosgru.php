<?php
require_once 'modales.php';
require_once 'modelos/GpPequemodelos.php';
$tipodoc = ModeloTipodoc();
$carreras = ModeloCarrera();
$cargo = ModeloCargogrupo();
$ciclos = ModeloCiclo();
$usuarios = $_SESSION["usuario"]["id_usuario"];
$saber = ModeloSabeCarrera($usuarios);
$lidersess=ModeloDatogrup($usuarios);
?>
<style>
    body{

    }
</style>
<script>
    $(document).ready(function () {
        datgrup(<?php echo $_SESSION["usuario"]["id_usuario"]; ?>);
        listardatainte(<?php echo $lidersess["id_grupo_periodo"] ;?>);
        tablesepersona(<?php echo $lidersess["id_grupo_periodo"] ;?>);
    });</script>
<!--Panel-->
<div class="" style="margin-top: -35px; background: #FFFFFF;">
    <div class=" primary-color-dark white-text" style="padding: 5px;"><center><h3><b>AGREGAR INTEGRANTES</b></h3></center></div>
    <div class="" style="background: #FFFFFF;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" data-numero="<?php echo $lidersess["id_grupo_periodo"] ;?>">

                    <div class="x_content" >
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 profile_left" >
                            <div class="profile_img">
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    <?php if ($saber["id_carrera_gp"] == 5) { ?>
                                        <center> <img class="img-responsive avatar-view img-thumbnail" src="vista/img/ambiental.jpg" alt="Avatar" title="Change the avatar"></center>
                                    <?php }if ($saber["id_carrera_gp"] == 8 || $saber["id_carrera_gp"] == 7 || $saber["id_carrera_gp"] == 9) { ?>
                                        <center> <img class="img-responsive avatar-view img-thumbnail" src="vista/img/cienciasempresariales.png" alt="Avatar" title="Change the avatar"></center>
                                    <?php }if ($saber["id_carrera_gp"] == 4) { ?>
                                        <center> <img class="img-responsive avatar-view img-thumbnail" src="vista/img/sistema.png" alt="Avatar" title="Change the avatar"></center>
                                    <?php }if ($saber["id_carrera_gp"] == 10) { ?>
                                        <center> <img class="img-responsive avatar-view img-thumbnail" src="vista/img/psicologia.png" alt="Avatar" title="Change the avatar"></center>
                                    <?php }if ($saber["id_carrera_gp"] == null) { ?>
                                        <center> <img class="img-responsive avatar-view img-thumbnail" src="vista/recursos/images/picture.jpg" alt="Avatar" title="Change the avatar"></center>
                                    <?php }if ($saber["id_carrera_gp"] == 6) { ?>
                                        <center> <img class="img-responsive avatar-view img-thumbnail" src="vista/img/fia.png" alt="Avatar" title="Change the avatar"></center>
                                    <?php } ?>

                                </div>
                            </div>
                            <center><a id="modidat" class="btn btn-success"  aria-hidden="true" data-toggle="modal" data-target="#modicadatgrupe" data-toggle="tooltip" data-placement="top"  title="EDITAR DATOS DE GRUPO"><i class="fa fa-edit m-right-xs"></i> EDITAR </a></center>
                            <ul id="datogrup" class="list-unstyled user_data" style="margin-top: 15px;">

                            </ul>

                        </div>
                        <div class="table-responsive col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <table id="tableintegrante"  class="table hover table-striped table-bordered " cellspacing="0" width="100%" >
                                <thead>
                                    <tr >
                                        <th class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center"> <a id="agregaper"  aria-hidden="true" data-toggle="modal" data-target="#sepersona" title="AGREGAR INTEGRANTE"> <i class="fa fa-plus-circle fa-lg" data-placement="top" data-toggle="tooltip" data-original-title="AGREGAR INTEGRANTE"></i></a> INTEGRANTES </th>
                                        <th class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center" aria-hidden="true"  >CARGO</th>
                                        <th class="">ACCIONES</th>


                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fin de modal de agregar escuela sabatica-->
<form id="seleccionaperso" name="seleccionaperso">
    <div class="modal fade " id="sepersona" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg " role="document" >
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100" id="myModalLabel"><center><b>AGREGAR INTEGRANTE</b></center></h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="text-right col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#basicExample">
                                    <i class="fa fa-edit m-right-xs"></i>  AGREGAR PERSONA
                                </button>


                            </div>
                        </div>
                        <input type="hidden" id="grupoperi" name="grupoperi">
                        <div class="table-responsive col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <table id="tablesepersona" class="table hover table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" style="margin-top: 15px;">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;" class="col-lg-1">FOTO</th>
                                        <th>NOMBRES</th>
                                        <th class="col-lg-4">ACCIONES</th>


                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <CENTER>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                    </CENTER>
                </div>

            </div>
            <!--/.Content-->
        </div>
    </div>
</form>


<!-- Modal -->
<div class="modal fade" id="ModalcargoInte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-success" role="document">
        <!--Content-->
        <form id="formcargo">
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100" id="myModalLabel"><center>CARGO</center></h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <label for="IGLESIA" id="" class="">CARGO:</label>
                    <input id="integrante_cargo" type="hidden">
                    <input id="registro" type="hidden">
                    <select id="cargos" class="form-control" required="">
                        <option disabled="" selected>SELECCIONAR CARGO</option>
                        <?php foreach ($cargo as $cargo) {
                            ?>
                                <option value="<?php echo $cargo["id_cargo"]; ?>"  style="font-size: 14px;"><?php echo $cargo["cargo"] ?></option>
                            <?php }
                        
                        ?>
                    </select>
                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <center><button type="submit" class="btn btn-primary">GUARDAR</button></center>
                </div>
            </div>
        </form>
        <!--/.Content-->
    </div>
</div>
<!-- Modal -->



<form id="formAddPerEstudiant"  enctype="multipart/form-data">
<div class="modal fade" id="basicExample" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel"><center>AGREGAR PERSONA</center></h4>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="md-form">
                            <input type="number" id="codigo" name="codigo" maxlength="9" class="form-control" placeholder="INGRESAR CODIGO">
                            <label for="CODIGO" class="">CODIGO:</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tnomgrupe">
                            <div class="md-form">
                                <input type="text" id="nomgrupe" name="nomgrupe" class="form-control" placeholder="INGRESAR NOMBRE">
                                <label for="GRUPO" class="">NOMBRE:</label>
                            </div>
                        </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tapepat">
                        <div class="md-form">
                            <input type="text" id="apepat" name="apepat" class="form-control" placeholder="INGRESAR APELLIDO PATERNO">
                            <label for="GRUPO" class="">APELLIDO PATERNO:</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tapemat">
                        <div class="md-form">
                            <input type="text" id="apemat" name="apemat" class="form-control" placeholder="INGRESAR APELLIDO MATERNO">
                            <label for="GRUPO" class="">APELLIDO MATERNO:</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4" id="ttipodocument">
                        <label for="IGLESIA" class="">TIPO DOCUMENTO:</label>
                        <div class="md-form">
                            <select id="tipodocument" name="tipodocument" class="chosen form-control" required="">
                                <option disabled selected>SELECCIONAR TIPO DOCUMENTO</option>
                                <?php
                                foreach ($tipodoc as $tipodoc) {
                                    ?>
                                    <option value="<?php echo $tipodoc["id_tipo_doc"]; ?>"><?php echo $tipodoc["abreviatura"]; ?></option>
<?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tnrodoc">
                        <div class="md-form">
                            <input type="number" id="nrodoc" name="nrodoc" maxlength="8" class="form-control" placeholder="INGRESAR NUMERO DE DOCUMENTO">
                            <label for="GRUPO" class="">NUMERO DE DOCUMENTO:</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4" id="tcarrera">
                        <label for="IGLESIA" class="">CARRERA:</label>
                        <div class="md-form">
                            <select id="carrera" name="carrera" class="chosen form-control" required="">
                                <option disabled selected>SELECCIONAR CARRERA</option>
                                <?php
                                foreach ($carreras as $carreras) {
                                    ?>
                                    <option value="<?php echo $carreras["id_carrera"]; ?>"><?php echo $carreras["abreviatura"]; ?></option>
<?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4" id="tciclo">
                        <label for="IGLESIA" class="">CICLO:</label>
                        <div class="md-form">
                            <select id="ciclo" name="ciclo" class="chosen form-control" required="">
                                <option disabled selected>SELECCIONAR CICLO</option>
                                <?php
                                foreach ($ciclos as $ciclos) {
                                    ?>
                                    <option value="<?php echo $ciclos["id_ciclo"]; ?>"><?php echo $ciclos["nro_romanos"]; ?></option>
<?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tfechanac">
                        <label for="GRUPO" class="">FECHA DE NACIMIENTO:</label>
                        <div class="md-form" style="margin-top: -25px;">
                            <input type="date" id="fechanac" name="fechanac" class="form-control" placeholder="INGRESAR FECHA DE NACIMIENTO">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="ttelefono">
                        <div class="md-form">
                            <input type="number" maxlength="9" id="telefono" name="telefono" class="form-control" placeholder="INGRESAR TELEFONO">
                            <label for="GRUPO" class="">TELEFONO:</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tcorreo">
                        <div class="md-form">
                            <input type="text" id="correo" name="correo" class="form-control" placeholder="INGRESAR CORREO">
                            <label for="GRUPO" class="">CORREO:</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="tdireccion">
                        <div class="md-form">
                            <input type="text" id="direccion" name="direccion" class="form-control" placeholder="INGRESAR DIRECCION">
                            <label for="GRUPO" class="">DIRECCION:</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <label for="GRUPO" class="">FOTO:</label>
                        <div class="md-form">

                            <input type="file" id="stude_img" name="stude_img" >

                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div id="gender" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-cyan" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                <input type="radio" name="genero"  id="genero" required="" value="M" >MASCULINO
                            </label>
                            <label class="btn btn-pink " data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                <input type="radio" name="genero" required="" id="genero" value="F" >FEMENINO
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <CENTER>
                    <button type="submit" class="btn btn-primary">REGISTRAR</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                </CENTER>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
</form>
<!-- Modal -->

<!--/.Panel-->
<form id="editardatogrupe" name="editardatogrupe">
    <div class="modal fade modicaldatgru" id="modicadatgrupe" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg " role="document" >
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100" id="myModalLabel"><center><b>REGISTRAR GRUPO PEQUEÑO</b></center></h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="id_grupo_periodo" name="id_grupo_periodo" class="form-control" >
                        <input type="hidden" id="id_lider" name="id_lider" class="form-control" >
                        <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6">

                            <div class="md-form">

                                <input type="text" id="lema" name="lema" class="form-control" placeholder="INGRESE LEMA">
                                <label for="CANTO" class="">LEMA:</label>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6">

                            <div class="md-form">
                                <input type="text" id="canto" name="canto" class="form-control" placeholder="INGRESE CANTO">
                                <label for="CANTO" class="">CANTO:</label>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6">

                            <div class="md-form">
                                <input type="text" id="versiculo" name="versiculo" class="form-control" placeholder="INGRESE VERSICULO">
                                <label for="CANTO" class="">VERSICULO:</label>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6">

                            <div class="md-form">
                                <input type="number" id="blanco_baut" name="blanco_baut" class="form-control" placeholder="INGRESE BAUTISMO">
                                <label for="CANTO" class="">BLANCO BAUTISMO:</label>
                            </div>

                        </div>
                    </div>

                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <CENTER>
                        <button type="submit" class="btn btn-primary">REGISTRAR</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                    </CENTER>
                </div>

            </div>
            <!--/.Content-->
        </div>
    </div>
</form>

<script type="text/javascript">

    $(".chosen").chosen({
        width: "100%",

        no_results_text: "NO HAY RESULTADO"
    });
</script>