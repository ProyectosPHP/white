<?php
$añoss = date("Y");

//$inicio = strftime("%A, %d de %B del %Y", strtotime("16-03-2018"));
//
//$anioinicio = strftime("%d de %B del %Y",strtotime("16-03-2018") );
//echo $anioinicio;
$hoy = date("Y-m-d");
$md = date("Y-m");
$semana = date("d");
$fechafin = date($md . '-' . ($semana + 7));
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-8">
                    <h3>MANTENIMIENTO SEMANA<small> ESCUELA SABATICA</small></h3>
                </div>
                <div class="col-md-2">
                    <select class="form-control" id="añoPeriodo" name="añoPeriodo">

                    </select>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#AgregarSemana"><i class="fa fa-plus-circle"></i> CREAR SEMANA</button>
                </div>
            </div>
            <div class="row table-responsive" >
                <table class="table table-bordered" id="TBSemana">
                    <thead>
                        <tr>
                            <th>SEMANA</th>
                            <th>LECCION</th>
                            <th>FECHA INICIO</th>
                            <th>FECHA FIN</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>

                </table>

            </div>



            <div class="clearfix"></div>
        </div>
    </div>


</div>

<!-- Modal -->


    <div class="modal fade" id="AgregarSemana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <!--Content-->
            <form id="FormAppSema">
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header" >

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title w-100" id="myModalLabel"><center><b>AGREGAR SEMANA</b></center></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="defaultFormCardNameEx" class="grey-text font-weight-light">PERIODO</label>
                                <select class="form-control" id="periodoAdd" name="PeriodoAdd">

                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label for="defaultFormCardNameEx" class="grey-text font-weight-light">SEMANA</label>
                                <input type="text" id="SEMANA" name="SEMANA" class="form-control" value="SEMANA" autofocus="">
                            </div>
                            <div class="col-lg-4">
                                <label for="defaultFormCardNameEx" class="grey-text font-weight-light" >LECCION</label>
                                <input type="text" id="LECCION" name="LECCION"class="form-control" value="LECCION" autofocus="">

                            </div>
                            <div class="col-lg-4">
                                <label for="defaultFormCardNameEx" class="grey-text font-weight-light">FECHA INICIO</label>
                                <input type="date" id="fechaini" name="fechaini" class="form-control" value="<?php echo $hoy; ?>">

                            </div>
                            <div class="col-lg-4">
                                <label for="defaultFormCardNameEx" class="grey-text font-weight-light">FECHA FIN</label>
                                <input type="date" id="fechafin" name="fechafin" class="form-control" value="<?php echo $fechafin; ?>">

                            </div>


                        </div>
                    </div>
                    <!--Footer-->
                    <div class="modal-footer">
                        <center>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> GUARDAR</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> CERRAR</button>
                        </center>
                    </div>
                </div>
            </form>
            <!--/.Content-->
        </div>
    </div>


