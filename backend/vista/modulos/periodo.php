
<?php
require_once 'modelos/GpPequemodelos.php';
require_once 'modales.php';
$periodo = ModeloPeriodo();
$EscuelaSabatica = ModeloEscuelaSabatica();
$GrupoPequeno = ModeloGrupoPequeno();
$iglesia = ModeloIglesia();
$carrera = ModeloCarrera();
$universidad = ModeloUniversida();
?>
<style>
    /*    @media screen and (max-width: 699px) and (min-width: 992px) {
            div#sectionciclo{
                margin-top: 100px;
            }
        }*/
    @media screen and (min-width: 992px) {
        select#sectionciclo{
            margin-top: 100px;
        }
    }
</style>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>2017-II</h2>
                <div class="nav navbar-right ">
                    <h2>IGLESIA / VILLA UNION</h2>   
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <center><h3>REGISTRO</h3></center>
                <br>
                <form  method="POST" action="controlador/ControlGrupoPeriodo.php?opcion=add" id="foraddgruperi">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6  col-lg-6">
                            <!--
                                                        <label class="control-label col-lg-12" for="first-name">NOMBRE G.P:
                                                        </label>-->

                            <label for="form41" class="">NOMBRE G.P: <a><i class="fa fa-plus-circle fa-lg" aria-hidden="true" data-toggle="modal" data-target="#Grupopeque"></i></a></label>
                            <div class="md-form">
                                <select id="id_grupo_pequeno" name="id_grupo_pequeno" class="chosen form-control colorful-select dropdown-info">
                                    <option disabled selected>SELECCIONE NOMBRE G.P </option>
                                    <?php foreach ($GrupoPequeno as $GrupoPequeno) { ?>
                                        <option value="<?php echo $GrupoPequeno["id_grupo_pequeno"]; ?>"><?php echo $GrupoPequeno["nombre"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6  col-lg-6">
                            <label for="IGLESIA" class="">ESCUELA SABATICA:</label>
                            <div class="md-form">
                                <select id="id_escuela_sabatica" name="id_escuela_sabatica" class="chosen form-control" >
                                    <option disabled selected>SELECCIONAR ESCUELA</option>
                                    <?php
                                    foreach ($EscuelaSabatica as $EscuelaSabatica) {
                                        ?>
                                        <option value="<?php echo $EscuelaSabatica["id_escuela_sabatica"]; ?>"><?php echo $EscuelaSabatica["escuela_sabatica"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6  col-lg-6">
                            <!--                            <label class="control-label" for="first-name">LEMA:
                                                        </label>-->
                            <div class="md-form">
                                <input type="text" id="lemas" name="lemas" class="form-control" placeholder="INGRESAR LEMA">
                                <label for="LEMA" class="">LEMA:</label>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6  col-lg-6">
                            <!--                            <label class="control-label" for="first-name">CANTO:
                                                        </label>-->
                            <div class="md-form">
                                <input type="text" id="canto" name="canto" class="form-control" placeholder="INGRESAR CANTO">
                                <label for="CANTO" class="">CANTO:</label>
                            </div>

                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-4  col-lg-4">
                            <!--                            <label class="control-label" for="first-name">VERSICULO:</label>-->

                            <div class="md-form">
                                <input type="text" id="versiculo" name="versiculo" class="form-control" placeholder="INGRESAR VERSICULO">
                                <label for="VERSICULO" class="">VERSICULO:</label>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-4  col-lg-4">
                            <!--                            <label class="control-label " for="first-name">BLANCO BAUT:
                                                        </label>-->
                            <div class="md-form">
                                <input type="number" id="blancobau" name="blancobau" class="form-control" placeholder="INGRESAR BLANCO BAUT">
                                <label for="BLANCO" class="">BLANCO BAUT:</label>
                            </div>
                        </div>
                        <div class=" col-xs-6 col-sm-6 col-md-4  col-lg-4">
                            <label for="FILIAL" class="">FILIAL:</label>
                            <div class="md-form">
                                <select id="universidad" name="universidad" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR FILIAL</option>
                                    <?php
                                    foreach ($universidad as $universidad) {
                                        ?>
                                        <option value="<?php echo $universidad["abreviatura"]; ?>"><?php echo $universidad["abreviatura"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!--                        <div class="col-xs-6 col-sm-6 col-md-4  col-lg-4" >
                                                    <label>CICLO</label>
                                                    <div class="md-form">
                                                    <select id="ciclo" name="ciclo" id="sectionciclo" class="chosen form-control colorful-select dropdown-info">
                                                        <option disabled selected> SELECCIONAR CICLO </option>
                        <?php // foreach ($periodo as $periodo) { ?>
                                                                <option value="<?php // echo $periodo["id_periodo"];  ?>"><?php // echo $periodo["nombre"];  ?></option>
                        <?php // } ?>
                                                    </select>
                                                    </div>
                                                </div>-->
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4">
                            <label for="IGLESIA" class="">CARRERA:</label>
                            <div class="md-form">
                                <select id="carrera" name="carrera" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR CARRERA</option>
                                    <?php
                                    foreach ($carrera as $carrera) {
                                        if ($carrera["id_univ_unidacad"] > 3) {
                                            ?>
                                            <option value="<?php echo $carrera["id_univ_unidacad"]; ?>"><?php echo $carrera["nombre"]; ?></option>
                                        <?php }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4">
                            <label for="IGLESIA" class="">IGLESIA:</label>
                            <div class="md-form">
                                <select id="iglesia" name="iglesia" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR IGLESIA</option>
                                    <?php
                                    foreach ($iglesia as $iglesia) {
                                        ?>
                                        <option value="<?php echo $iglesia["id_iglesia"]; ?>"><?php echo $iglesia["nombre"]; ?></option>
<?php } ?>
                                </select>
                            </div>
                        </div>
                        <!--                        <div class="col-xs-6 col-sm-6 col-md-4  col-lg-4">
                                                                                <label class="control-label" for="first-name">FECHA:
                                                                                </label>
                                                                                <input type="date" id="first-name" required="required" class="form-control">
                        
                                                    <div class="md-form">
                                                        <input placeholder="SELECCIONAR FECHA" id="fecha" name="fecha" length="10" type="text" id="date-picker-example" class="form-control datepicker timepicker">
                                                        <label for="date-picker-example">FECHA:</label>
                                                    </div>
                                                    <script type="text/javascript">
                                                        $(document).ready(function () {
                        
                                                            $('.datepicker').pickadate({
                                                                weekdaysShort: ['DOMINGO', 'LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO'],
                                                                monthsShort: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
                                                                monthsFull: ['ENERO', 'FEBRERO', 'ABRIL', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIENBRE', 'DICIENBRE'],
                                                                formatSubmit: 'yyyy/mm/dd',
                                                                today: 'HOY',
                                                                clear: '',
                                                                format: 'dd/mm/yyyy',
                                                                showMonthsShort: undefined,
                                                                showWeekdaysFull: undefined,
                                                                labelMonthNext: 'SIGUIENTE MES',
                                                                labelMonthPrev: 'ANTERIOR MES',
                                                                labelMonthSelect: 'SELECCIONAR MES',
                                                                labelYearSelect: 'SELECCIONAR AÑO',
                                                                selectMonths: true,
                                                                selectYears: true,
                                                                selectYears: 10,
                                                                firstDay: undefined,
                        
                                                            });
                                                        });</script>
                                                </div>-->
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                            <center>
                                <button class="btn btn-primary" type="submit" id="btncreargp"><i class="fa fa-floppy-o" aria-hidden="true"></i> REGISTRAR</button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> <script type="text/javascript">
//                                $(document).ready(function () {
//                                    $('.mdb-select').material_select();
//                                });
        $(".chosen").chosen({
            width: "100%",

            no_results_text: "NO HAY RESULTADO"
        });
    </script>