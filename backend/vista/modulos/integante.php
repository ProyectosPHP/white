<?php
require_once 'modelos/GpPequemodelos.php';
$carrera = ModeloCarrera();
?>
<!--Panel-->
<div class="card" style="margin-top: 15px; background: #FFFFFF;">
    <h3 class="card-header primary-color white-text" style="padding: 5px;">
        <center><h3>REGISTRO INTEGRANTES</h3></center>
    </h3>
    <div class="card-block container" style="background: #FFFFFF;">
        <div class="row col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <label for="form41" class="">GRUPO PERIODO:</label>
                <div class="md-form">
                    <select id="id_grupo_pequeno" name="id_grupo_pequeno" class="chosen form-control colorful-select dropdown-info">
                        <option disabled selected>SELECCIONE GRUPO PERIODO</option>
                        <?php // foreach ($GrupoPequeno as $GrupoPequeno) { ?>
                        <option value="<?php // echo $GrupoPequeno["id_grupo_pequeno"];   ?>"><?php // echo $GrupoPequeno["nombre"];   ?></option>
                        <?php // } ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <label for="form41" class="">INTEGRANTE:</label>
                <div class="md-form">
                    <select id="id_grupo_pequeno" name="id_grupo_pequeno" class="chosen form-control colorful-select dropdown-info">
                        <option disabled selected>SELECCIONE INTEGRANTE</option>
                        <?php // foreach ($GrupoPequeno as $GrupoPequeno) { ?>
                        <option value="<?php // echo $GrupoPequeno["id_grupo_pequeno"];   ?>"><?php // echo $GrupoPequeno["nombre"];   ?></option>
                        <?php // } ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <label for="form41" class="">CARRERA:</label>
                <div class="md-form">
                    <select id="id_grupo_pequeno" name="id_grupo_pequeno" class="chosen form-control colorful-select dropdown-info">
                        <option disabled selected>SELECCIONE CARRERA </option>
                        <?php
                        foreach ($carrera as $carrera) {
                            if ($carrera["id_univ_unidacad"] > 3) {
                                ?>
                                <option value="<?php echo $carrera["id_univ_unidacad"]; ?>"><?php echo $carrera["nombre"]; ?></option>
                            <?php
                            }
                        }
                        ?>

                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <label for="form41" class="">CICLO:</label>
                <div class="md-form">
                    <select id="id_grupo_pequeno" name="id_grupo_pequeno" class="chosen form-control colorful-select dropdown-info">
                        <option disabled selected>SELECCIONE CICLO </option>
                        <?php // foreach ($GrupoPequeno as $GrupoPequeno) {  ?>
                        <option value="<?php // echo $GrupoPequeno["id_grupo_pequeno"];  ?>"><?php // echo $GrupoPequeno["nombre"];  ?></option>
<?php // }  ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="md-form">
                    <input type="text" id="blancobau" name="blancobau" class="form-control" placeholder="INGRESAR TIPO INTEGRANTE">
                    <label for="BLANCO" class="">TIPO INTEGRANTE</label>
                </div>
            </div>
        </div>
        <div class="row">
            <center>  <a class="btn btn-elegant btn-lg"><i class="fa fa-floppy-o" aria-hidden="true"></i> REGISTRAR</a></center>
        </div>
        <div class="row">
            <br>
        </div>
    </div>
</div>
<!--/.Panel-->
<script type="text/javascript">
//                                $(document).ready(function () {
//                                    $('.mdb-select').material_select();
//                                });
    $(".chosen").chosen({
        width: "100%",

        no_results_text: "NO HAY RESULTADO"
    });
</script>