<?php 
require_once 'modelos/GpPequemodelos.php';
$escuela= ModeloAdminEscc();
$carreras= ModeloCarrera();
$GrupoPequeno= ModeloGrupoPequeno();
$universidad= ModeloUniversida();
$persona= ModeloPersona();
require_once 'modales.php';
?>
<style>

</style>
<!--Panel-->
<div class="card" style="margin-top: 15px; background: #FFFFFF;">
    <div class="card-header primary-color-dark white-text" style="padding: 5px;"><center><h3><b>CONFIGURACION DE ES</b></h3></center></div>
    <div class="card-block" style="background: #FFFFFF;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form>
                        <div class="row">
                            <input id="idesc" type="hidden">
                            <input id="nombreescu" type="hidden">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-6">
                                
                                <label for="IGLESIA" class="">ESCUELA SABATICA:</label>
                                <select id="es" name="es" class=" form-control" required="">
                                    <option value="">ESCUELA SABATICA</option>
                                    <?php foreach ($escuela as $escuela){ ?>
                                    <option value="<?php echo $escuela["id_escuela_sabatica"]; ?>"><?php echo $escuela["nombre"] ?></option>
                                    <?php }?>
                                </select>
                                <br>
                            </div>
                            <div class="col-xs-2 col-lg-4"></div>
                            <div class="col-xs-8 col-sm-6 col-md-6 col-lg-4 col-xl-6">
                                <div class="text-right col-xs-9 ">
                                    <a class="btn btn-primary waves-effect waves-light" id="agregardatos" aria-hidden="true" data-toggle="modal"  disabled="disabled"> <i class="fa fa-plus-circle" aria-hidden="true"></i> AGREGAR GP</a>
                                </div>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                        </form>
                        <div class="col-sm-12 table-responsive">
                            <center>
                            <table id="grupp" class="table hover table-striped table-bordered nowrap " cellspacing="0" width="100%" style="margin-top: 15px;">
                                <thead>
                                    <tr>
                                        <!--<th >#</th>-->
                                        <th>GRUPO PEQUEÑO</th>
                                        <th>ESCUELA</th>
                                        <th>CARRERA</th>
                                        <th>LIDER</th>
                                        
                                    </tr>
                                </thead>
                            </table>
                            </center>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/.Panel-->
<script type="text/javascript">
    $(".chosen").chosen({
        width: "100%",

        no_results_text: "NO HAY RESULTADO"
    });
</script>
<div class="modal fade top modal-success" id="modalescuelasab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document" >
        <!--Content-->
        <div class="modal-content" >
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel"><center><b>REGISTRAR G.P EN <span id="nombreescuela"></span> </b></center></h4>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="idescuela" name="idescuela">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="text-right col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <button class="btn btn-primary waves-effect waves-light"  aria-hidden="true" data-toggle="modal" data-target="#grupoadd"> <i class="fa fa-plus-circle" aria-hidden="true"></i> AGREGAR NUEVO GP</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <table id="selecgrupope" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" style="margin-top: 15px;">
                            <thead>
                                <tr>
                            
                                    <th>GRUPO PEQUEÑO</th>
                                    <th style="width: 20px;">ACCIONES</th>

<!--<th style="width:150px;">ACCION</th>-->
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <CENTER>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                </CENTER>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--fin de modal de agregar escuela sabatica-->

<!--modal seleccionar agregar gruupo-->
<div class="modal fade top modal-success" id="grupoadd" tabindex="-3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document" >
        <!--Content-->
        <div class="modal-content" >
            <form id="foraddgruperis">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100" id="myModalLabel"><center><b>REGISTRAR GRUPO PEQUEÑO</b></center></h4>
                </div>
                <!--Body-->
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                            <div class="md-form">
                                <input type="text" id="nomgrupe" name="nomgrupe" class="form-control" placeholder="INGRESAR NOMBRE">
                                <label for="GRUPO" class="">NOMBRE DE GRUPO PEQUEÑO:</label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4">
                            <label for="IGLESIA" class="">LIDER:</label>
                            <div class="md-form">
                                <select id="lider" name="lider" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR LIDER</option>
                                    <?php
                                    foreach ($persona as $persona) {
                                        ?>
                                        <option value="<?php echo $persona["id_persona"]; ?>"><?php echo $persona["nombres"]; ?> <?php echo $persona["apepat"]; ?> <?php echo $persona["apemat"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4  col-lg-4">
                            <label for="IGLESIA" class="">CARRERA:</label>
                            <div class="md-form">
                                <select id="carrera" name="carrera" class="chosen form-control" required="">
                                    <option disabled selected>SELECCIONAR CARRERA</option>
                                    <?php
                                    foreach ($carreras as $carreras) {
                                        
                                            ?>
                                            <option value="<?php echo $carreras["id_carrera"]; ?>"><?php echo $carreras["abreviatura"]; ?></option>
                                            <?php
                                        
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                  
                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <CENTER>
                        <button class="btn btn-primary" type="submit" id="btncreargp"><i class="fa fa-floppy-o" aria-hidden="true"></i> REGISTRAR</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
                    </CENTER>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--fin de modal de agregar escuela sabatica-->
<script type="text/javascript">
    $(".chosen").chosen({
        width: "100%",

        no_results_text: "NO HAY RESULTADO"
    });
</script>

