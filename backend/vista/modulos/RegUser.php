<div class="card" style="margin-top: 15px; background: #FFFFFF;">
    <div class="card-header primary-color-dark white-text" style="padding: 5px;"><center><h3><b>REPORTE USUARIO</b></h3></center></div>
    <div class="card-block" style="background: #FFFFFF;">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        
                        <div class="col-sm-12 table-responsive">
                            <center>
                            <table id="TbRepUser" class="table hover table-striped table-bordered nowrap " cellspacing="0" width="100%" style="margin-top: 15px;">
                                <thead>
                                    <tr>
                                        
                                        <th>LOGIN</th>
                                        <th>PERSONA</th>
                                        <th>ROL</th>
                                    </tr>
                                </thead>
                            </table>
                            </center>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>