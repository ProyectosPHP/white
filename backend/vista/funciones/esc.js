$(document).ready(function () {
    $('#agregardatos').click(function () {
        var id = $("#idesc").val();
        var nombre = $("#nombreescu").val();
        if (id !== '' && nombre !== '') {
            $('#nombreescuela').text(nombre.toUpperCase());
            $('#idescuela').val(id);
            listargp(id);
        } else {
            swal(
                    'ADVERTENCIA!',
                    'SELECCIONE ESCUELA SABATICA',
                    'warning'
                    );
        }

    });

    $(document).on('change', '#es', function () {

        var idescuela = $(this).val();
        if (idescuela === '') {
            listargpe(0);
        }
        if (idescuela !== null) {
            listargpe(idescuela);
            $.ajax({
                type: 'POST',
                url: "ajax/grupopeque.php?opcion=DatosEscuel",
                async: false,
                data: {idescuela: idescuela},
                error: function () {
                    swal(
                            'ERROR!',
                            'PAGINA NO ENCONTRADA',
                            'error'
                            );
                }, success: function (data) {
                    var datos = JSON.parse(data);
                    $("#nombreescu").val(datos[0].nombre);
                    $("#idesc").val(datos[0].id_escuela_sabatica);
                    $('#agregardatos').removeAttr("disabled");
                    $('#agregardatos').attr("data-target", "#modalescuelasab");
//                      $("#modalescuelasab").modal("show");
                }
            });

        } else {
            listargpe(0);
            $('#agregardatos').attr("disabled", "disabled");
            $('#agregardatos').removeAttr("data-target");
            $('#idesc').val('');
            $('#nombreescu').val('');

        }
    });

    $(document).on('change', '#permiso', function () {
        var permiso = $(this).is(":checked");
        var id_rol_opcion = $(this).data("numero");
        $.ajax({
            type: 'POST',
            async: false,
            url: "ajax/modulos.php",
            data: {permiso: permiso, id_rol_opcion: id_rol_opcion},
            error: function () {
                swal(
                        'ERROR!',
                        'PAGINA NO ENCONTRADA',
                        'error'
                        );
            }, success: function (data) {

            }
        });

    });
    $('#FormUdpEsc').on('submit', function () {
        var idescue = $('#d').val();
        var manescue = $('#manescue').val();
        var facultadup = $('#facultadup').val();
        $.ajax({
            type: 'POST',
            async: false,
            url: "ajax/escuela.php?opcion=UDP",
            data: {d: idescue, manescue: manescue, facultadup: facultadup},

            success: function (data) {

                var dato = JSON.parse(data);
                if (dato[0].alert === "success") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    TablaManEsc();
                }
                if (dato[0].alert === "warning") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    return false;
                }



            }
        });
        return false;
    });

    $('#FormAddEscssss').on('submit', function () {
        var manescue = $('#escuela').val();
        var facultadup = $('#id_facultad').val();

        $.ajax({
            type: 'POST',
            async: false,
            url: "ajax/escuela.php?opcion=ADD",
            data: {escuela: manescue, id_facultad: facultadup},

            success: function (data) {
                
                var dato = JSON.parse(data);
                if (dato[0].alert === "success") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    TablaManEsc();
                    $("#FormAddEscssss")[0].reset();
                }
                if (dato[0].alert === "warning") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    return false;
                }



            }
        });
        return false;
    });

});
function agregar(id) {
    var idescu = $('#idescuela').val();
    $.ajax({
        type: 'POST',
        url: "controlador/escuela.php",
        async: false,
        data: {id_escuela_sabatica: idescu, id_grupo_pequeno: id},
        error: function () {
            swal(
                    'ERROR!',
                    'PAGINA NO ENCONTRADA',
                    'error'
                    );
        }, success: function (data) {

            if (data === "SI") {
                swal(
                        'OK!',
                        'SU GRUPO FUE AGREGADO CORECTAMENTE',
                        'success'
                        );
                listargpe(idescu);
                listargp(idescu);
            }
            if (data === "NO") {
                swal(
                        'ERROR!',
                        'ERRO AL AGREGAR EL GRUPO PEQUEÑO',
                        'error'
                        );
            }
        }

    });
}
;