$(document).ready(function () {
    $('#foraddgruperi').on("submit", function (e) {
        e.preventDefault();
        var id_grupo_pequeno = $('#id_grupo_pequeno').val();
        var id_escuela_sabatica = $('#id_escuela_sabatica').val();
        var lema = $('#lemas').val();
        var canto = $('#canto').val();
        var versiculo = $('#versiculo').val();
        var blancobau = $('#blancobau').val();
        var universidad = $('#universidad').val();
        var carrera = $('#carrera').val();
        var iglesia = $('#iglesia').val();
      if (id_grupo_pequeno !== null && id_escuela_sabatica !== null && lema !== null && canto !== null && versiculo !== null && blancobau !== null && iglesia !== null && universidad !== null && carrera !== null) {

            $.ajax({
                type: 'POST',
                url: "controlador/ControlGrupoPeriodo.php?opcion=add",
                data: "id_grupo_pequeno=" + id_grupo_pequeno + "&id_escuela_sabatica=" + id_escuela_sabatica + "&lemas=" + lema + "&canto=" + canto + "&versiculo=" + versiculo +
                        "&blancobau=" + blancobau + "&universidad=" + universidad + "&carrera=" + carrera,
          error: function () {
                    swal(
                            'ERROR!',
                            'PAGINA NO ENCONTRADA',
                            'warning'
                            );
                }, success: function (data) {
                    if (data === "OK") {
                        swal(
                                'EXCELENTE!',
                                'EL GRUPO FUE AGREGADO CORRECTAMENTE',
                                'success'
                                );
                         $('#id_grupo_pequeno').val('');
                         $('#id_escuela_sabatica').val('');
                        $('#lemas').val('');
                        $('#canto').val('');
                        $('#versiculo').val('');
                        $('#blancobau').val('');
                        $('#universidad').val('');
                        $('#carrera').val('');
                        $('#iglesia').val('');
                    }
                    if (data === "NO") {
                        
                        swal(
                                'ERROR!!',
                                'ERROR AL AGREGAR LOS DATOS',
                                'error'
                                );
                         $('#id_grupo_pequeno').val('');
                         $('#id_escuela_sabatica').val('');
                        $('#lemas').val('');
                        $('#canto').val('');
                        $('#versiculo').val('');
                        $('#blancobau').val('');
                        $('#universidad').val('');
                        $('#carrera').val('');
                        $('#iglesia').val('');
                    }

                }
            });

        } else {
            swal(
                    'ERROR!',
                    'POR FAVOR LLENE EL FORMULARIO!!',
                    'warning'
                    );
        }

        return false;
    });
$("#formAddPerEstudiant").on("submit", function (e) {
        e.preventDefault();
       
        var formulario=document.getElementById("formAddPerEstudiant")[0];
        var codigo = $("#codigo").val();
        var nomgrupe = $("#nomgrupe").val();
        var apepat = $("#apepat").val();
        var genero = $("#genero").val();
        
        
        if (codigo !== "" && nomgrupe !== "" && apepat !== "" ) {
            if (formulario.genero[0].checked===true ||formulario.genero[1].checked===true) {
                 var fordatas = new FormData($('#formAddPerEstudiant')[0]);
                 $.ajax({
                type: 'POST',
                async: false,
                url: "ajax/PersonaIn.php?opcion=ADD",
                data: fordatas,
                contentType: false,
                processData: false,
                success: function (data) {
                    
                    var dato = JSON.parse(data);

                    if (dato[0].alert === "success") {
                        swal(
                                '',
                                dato[0].mensaje + '¡',
                                dato[0].alert
                                );

                       $('#formAddPerEstudiant')[0].reset();
                        tablesepersona();
                    }
                    if (dato[0].alert === "warning") {
                        swal(
                                '',
                                dato[0].mensaje + '¡',
                                dato[0].alert
                                );
                        return false;
                    }

                }
            });
            }else{
                swal(
                    'ALERTAR',
                    'SELECCIONE GENERO',
                    'success'
                    );
            }
           
        } else {
            swal(
                    'ALERTAR',
                    'POR FAVOR LLENE EL FORMULARIO',
                    'success'
                    );
        }


    });
});

