$(document).ready(function () {
    $('#modidat').click(function (e) {
        e.preventDefault();
        var id = $("#idgru").data("numero");
        $.ajax({
            type: 'POST',
            url: "ajax/grupopeque.php?opcion=edge",
            data: {"id": id},
            async: false,
            error: function () {
                swal(
                        'ERROR!',
                        'PAGINA NO ENCONTRADA',
                        'error'
                        );
            }, success: function (data) {
                var conten = JSON.parse(data);
                var modulos = conten['data'];
                for (var i = 0; i < (modulos.length); i++) {
                    var id_grupo_periodo = modulos[i]['id_grupo_periodo'];
                    var lema = modulos[i]['lema'];
                    var id_lider = modulos[i]['id_lider'];
                    var canto = modulos[i]['canto'];
                    var versiculo = modulos[i]['versiculo'];
                    var blanco_baut = modulos[i]['blanco_baut'];
                }
                $('#id_grupo_periodo').val(id_grupo_periodo);
                $('#lema').val(lema);
                $('#canto').val(canto);
                $('#versiculo').val(versiculo);
                $('#blanco_baut').val(blanco_baut);
                $('#id_lider').val(id_lider);

            }
        });
    });
    $('#editardatogrupe').on("submit", function (e) {
        e.preventDefault();
        var id_grupo_periodo = $('#id_grupo_periodo').val();
        var lema = $('#lema').val();
        var canto = $('#canto').val();
        var versiculo = $('#versiculo').val();
        var blanco_baut = $('#blanco_baut').val();
        var id_lider = $('#id_lider').val();

        $.ajax({
            type: 'POST',
            url: "controlador/ControlAgregrup.php?opcion=moddatgrup",
            data: {'id_grupo_periodo': id_grupo_periodo, 'lema': lema, 'canto': canto, 'versiculo': versiculo, 'blanco_baut': blanco_baut},
            error: function () {
                swal(
                        'ERROR!',
                        'PAGINA NO ENCONTRADA',
                        'error'
                        );
            }, success: function (data) {

                if (data === "OK") {
                    swal('¡OK!', 'Se han guardado los cambios correctamente!', 'success');
                    datgrup(id_lider);
//                    $("#modicadatgrupe").modal("hide");
//                    alert(blanco_baut);
                }
                if (data === "NO") {
                    swal(
                            'ERROR!',
                            'AL AGREGAR EL GRUPO',
                            'error'
                            );
                    alert(blanco_baut);

                }
            }
        });


    });
});
function evagrup(id) {

    var link = $.ajax({
        type: 'POST',
        url: "ajax/grupopeque.php?opcion=evaluar",
        data: {id: id},
        dataType: 'text',
        async: false
    }).responseText;
    document.getElementById("datogrup").innerHTML = link;

}
function datgrup(id) {

    var link = $.ajax({
        type: 'POST',
        url: "ajax/grupopeque.php?opcion=dg",
        data: {"id": id},
        dataType: 'text',
        async: false
    }).responseText;
    document.getElementById("datogrup").innerHTML = link;

}
function EditarManEscuela(id, escue, carre) {
    $("#FormUdpEsc #manescue").val(escue);
    $("#FormUdpEsc #d").val(id);
    $("#FormUdpEsc #facultadup").val(carre);
}

function DelManEscuela(id) {
    swal({
        title: 'Estás seguro?',
        text: "No podrás revertir esto!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI,ELIMINAR!',
        cancelButtonText: 'NO, CANCELAR!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: 'POST',
            async: false,
            url: "ajax/escuela.php?opcion=DEL",
            data: {id: id},

            success: function (data) {
                
                var dato = JSON.parse(data);
                if (dato[0].alert === "success") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    TablaManEsc();
                }
                if (dato[0].alert === "warning") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    return false;
                }



            }
        });
    });



}