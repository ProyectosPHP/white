$(document).ready(function () {
    $("#adminrol").on('change', function () {
        var ids = $(this).val();
        if (ids === "1" || ids === "2" || ids === "3" || ids === "4" || ids === "10" || ids === "11") {
            $("#tcodigo").addClass("hidden");
            $("#tcarrera").addClass("hidden");
            $("#tciclo").addClass("hidden");
            $("#caso").val(1);

        } else {
            $("#tcodigo").removeClass("hidden");
            $("#tcarrera").removeClass("hidden");
            $("#tciclo").removeClass("hidden");
            $("#caso").val(2);

        }
    });
    $("#padminrol").on('change', function () {
        var ids = $(this).val();
        tbmodulosperfil(ids);
        vermodulosperfil(ids);
    });
    $("#foraddusuario").on("submit", function (e) {
        e.preventDefault();
        var fordata = new FormData($('#foraddusuario')[0]);
        var ids = $("#adminrol").val();
        var usuario = $("#usuario").val();
        var clave = $("#clave").val();
        var codigo = $("#codigo").val();
        var nomgrupe = $("#nomgrupe").val();
        var apepat = $("#apepat").val();
        var apemat = $("#apemat").val();
        var tipodocument = $("#tipodocument").val();
        var nrodoc = $("#nrodoc").val();
        var carrera = $("#carrera").val();
        var ciclo = $("#ciclo").val();
        var fechanac = $("#fechanac").val();
        var telefono = $("#telefono").val();
        var correo = $("#correo").val();
        var direccion = $("#direccion").val();
        var fotousuario = $("#fotousuario").val();
        var genero = $("#genero").val();
        var user_image = $("#user_image").val();
        if (ids === "1" || ids === "2" || ids === "3" || ids === "4" || ids === "10" || ids === "11") {
            if (usuario !== '' && clave !== '') {
                enviardatos(fordata);
            } else {
                swal('PRECAUCIÓN',
                        'POR FAVOR LLENE EL FORMULARIO!',
                        'warning');
            }
        } else {
            if (usuario !== '' && clave !== '' && codigo !== '') {
                enviardatos(fordata);
            } else {
                swal(
                        'PRECAUCIÓN',
                        'POR FAVOR LLENE EL FORMULARIO!',
                        'warning'
                        );
            }
        }
    });
    
});

function enviardatos(enviardatosajax) {
    $.ajax({
        type: 'POST',
        async: false,
        url: "ajax/usuario.php?opcion=ADD",
        data: enviardatosajax,
        contentType: false,
        processData: false,
        success: function (data) {
            var dato = JSON.parse(data);
            if (dato[0].alert === "success") {
                swal(
                        '',
                        dato[0].mensaje + '¡',
                        dato[0].alert
                        );

                $('#foraddusuario')[0].reset();
            }
            if (dato[0].alert === "warning") {
                swal(
                        '',
                        dato[0].mensaje + '¡',
                        dato[0].alert
                        );
                return false;
            }



        }
    });
}



function eliminaritemperfil(id) {
    var rol = $('#padminrol').val();
    swal({
        title: 'ESTAS SEGURO?',
        text: "NO PODRAS REVERTIR ESTO!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI,ELIMINAR!',
        cancelButtonText: 'No,CANCELAR!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger'
    }).then(function () {
        $.ajax({
            type: 'POST',
            async: false,
            url: "ajax/perfil.php?opcion=DEL",
            data: {ID: id},
            success: function (data) {

                if (data === "OK") {
                    alertify.success("EL MODULO SE ELIMINO CORRECTAMENTE");

                }
                if (data === "ERROR") {
                    alertify.success("ERROR AL ELIMINAR EL MODULO");
                }

                tbmodulosperfil(rol);
                vermodulosperfil(rol);

            }
        });

    });
}


function  vermodulosperfil(id) {
    $.ajax({
        type: 'POST',
        async: false,
        url: "ajax/perfil.php?opcion=TM",
        data: {idmodulosperfil: id},
        success: function (data) {

            $("#mostrarmodulosperfil").html(data);


        }
    });
}
;
function addmodul(id) {
    var rol = $('#padminrol').val();

    $.ajax({
        type: 'POST',
        async: false,
        url: "ajax/perfil.php?opcion=ADD",
        data: {rol: rol, modulo: id},
        success: function (data) {
            tbmodulosperfil(rol);
            vermodulosperfil(rol);
        }
    });

}

var tbmodulosperfil = function (id) {
    var table = $('#pefilmodulo').DataTable({
        "destroy": true,
        "autoFill": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/perfil.php?opcion=T',
            'data': {idmodulosperfil: id}
        },
        "order": [],
        "columns": [
            {'data': 'nombre'},
            {'data': 'boton'}
        ],
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "columnDefs": [
            {
                "data": null,
                "title": "<center>ACCIONES</center>", "targets": 0,
                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });
};
