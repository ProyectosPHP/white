$(document).ready(function () {
    $('#frmvalida').on("submit", function (e) {
//    $('#btnloguea').click(function (e) {
        e.preventDefault();
        var usuario = document.getElementById('correo').value;
        var clave = document.getElementById('clave').value;
//        var expresion = /^[a-zA-Z0-9]*$/;
        if (usuario !== "" && clave !== "") {
            $.ajax({
                type: 'POST',
                url: "controlador/prueba.php",
                async: false,
                data: 'correo=' + usuario + '&clave=' + clave,
                beforeSend: function () {
                    $('#btnloguea').html("enviando.....");
                    $('#btnloguea').attr("disabled", "disabled");
                },
                success: function (data) {
                    
                    if (data === "no") {
                        $("body").overhang({
                            type: "error",
                            message: "EL USUARIO O LA CONTRASEÑA NO COINCIDEN",
                            callback: function () {
                                $('#btnloguea').html('<i class="fa fa-sign-in" aria-hidden="true"></i> INGRESAR');
                                $('#btnloguea').removeAttr("disabled");
                            }
                        });

                    }
                    if (data === "su") {
                        $("body").overhang({
                            type: "info",
                            message: "Su cuenta está suspendida.Para más informes pongasé en contacto con el administrador del Sistema",
                            callback: function () {
                                $('#btnloguea').html('<i class="fa fa-sign-in" aria-hidden="true"></i> INGRESAR');
                                $('#btnloguea').removeAttr("disabled");
                            }
                        });

                    }
                    if (data === "si") {
                        $("body").overhang({
                            type: "success",
                            message: "POR FAVOR ESPERE, REDIRIGIENDO...",
                            callback: function () {
                                window.location.href="backend";
                                $('#btnloguea').html('<i class="fa fa-sign-in" aria-hidden="true"></i> INGRESAR');
                                $('#btnloguea').removeAttr("disabled");
                                
                            }
                        });
                    }

                },
                error: function () {
                    $("body").overhang({
                        type: "error",
                        message: "PAGINA NO ENCONTRADA"
                    });
                }
            });

        } else {
            $("body").overhang({
                type: "warn",
                html: true,
                message: '<i class="fa fa-exclamation-triangle fa-lg" style="color:red;" aria-hidden="true"></i> POR FAVOR LLENE TODO SUS DATOS',
                duration: 3
            });
        }

    });





});