$(document).ready(function () {
    AP();
    MpAdd();


    $("#añoPeriodo").change(function () {
        var anio = $(this).val();
        tablesemana(anio);
    });

    listargpe(0);
    TablaReporUser();
    TablaManEsc();
    $(document).on('click', '.previous', function () {
        var post_id = $(this).attr("id");
        fetch_post_data(post_id);
    });

    $(document).on('click', '.next', function () {
        var post_id = $(this).attr("id");
        fetch_post_data(post_id);
    });

    $("#FormAppSema").submit(function (e) {
        e.preventDefault();
        var periodoAdd = $("#periodoAdd").val();
        var SEMANA = $("#SEMANA").val();
        var LECCION = $("#LECCION").val();
        var fechaini = $("#fechaini").val();
        var fechafin = $("#fechafin").val();
        var año=$("#añoPeriodo").val();
        $.ajax({
            type: 'POST',
            url: "ajax/grupopeque.php?opcion=SemanaAdd",
            data: {periodo: periodoAdd, fecha_ini: fechaini, fecha_fin: fechafin},
            success: function (data) {
                var dato = JSON.parse(data);
                var datos = dato["data"];
                if (datos[0].Estado === "NO") {
                    swal('',
                            datos[0].mensaje + '¡',
                            datos[0].alert
                            );

                }

                if (datos[0].Estado === "OK") {
                    $.ajax({
                        type: 'POST',
                        url: "ajax/grupopeque.php?opcion=SemanaADDD",
                        async: false,
                        data: {periodo: periodoAdd, SEMANA: SEMANA, LECCION: LECCION, fecha_ini: fechaini, fecha_fin: fechafin},
                        success: function (data) {
                            var dato = JSON.parse(data);
                            var datos = dato["data"];
                            swal('',
                                    datos[0].mensaje + '¡',
                                    datos[0].alert
                                    );
                            if(datos[0].Estado==="OK"){
                                tablesemana(año);
                                $("#FormAppSema")[0].reset();
                            }
                            
                            
                        }
                    });
                }





            }
        });












        return  false;

    });




    $('#formcargo').on('submit', function (e) {
        e.preventDefault();
        var id_cargo = $('#cargos').val();
        var integrante_cargo = $('#integrante_cargo').val();
        var registro = $('#registro').val();
        var u = $(".x_panel").data("numero");

        $.ajax({
            type: 'POST',
            url: "ajax/datosgrup.php?opcion=UDPCA",
            data: {id_cargo: id_cargo, inteCargo: integrante_cargo, usuario: u, registro: registro},
            async: false,
            success: function (data) {
                var dato = JSON.parse(data);
                if (dato[0].alert === "success") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    listardatainte(u);
                }
                if (dato[0].alert === "warning") {
                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    return false;
                }

            }
        });
    });



});
var idioma_espanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ning&uacuten dato disponible en esta tabla ,por favor introducir datos",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var TablaReporUser = function () {
    var table = $('#TbRepUser').DataTable({
        "destroy": true,
        "autoFill": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/escuela.php?opcion=RepUs'
        },
        "order": [],
        "columns": [
            {'data': 'login'},
            {'data': 'persona'},
            {'data': 'rol'}
//            {'data': 'estado',
//                "render": function (data) {
//                    if (data === "1") {
//                        return '<center><button type="button" class="btn btn-outline-success waves-effect">REVISADO</button></center>';
//                    }
//                    if (data === "0") {
//                        return '<center><button type="button" class="btn btn-outline-danger waves-effect" >FALTA REVISAR</button></center>';
//                    }
//
//                }
//            }

        ],
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "columnDefs": [
            {
                "data": null,

                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });

};


var TablaManEsc = function () {
    var table = $('#TbEscuela').DataTable({
        "destroy": true,
        "autoFill": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/escuela.php?opcion=L'
        },
        "order": [],
        "columns": [
            {'data': 'escuela'},
            {'data': 'facultad'},
            {'data': 'boton'}
//            {'data': 'estado',
//                "render": function (data) {
//                    if (data === "1") {
//                        return '<center><button type="button" class="btn btn-outline-success waves-effect">REVISADO</button></center>';
//                    }
//                    if (data === "0") {
//                        return '<center><button type="button" class="btn btn-outline-danger waves-effect" >FALTA REVISAR</button></center>';
//                    }
//
//                }
//            }

        ],
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "columnDefs": [
            {
                "data": null,

                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });

};
var validardatos = function () {
    $.ajax({
        url: "ajax/grupopeque.php?opcion=gp",
        error: function () {
            alert("PAGINA NO ENCONTRADA");
        },
        success: function (data) {
            alert(data);
        }
    });
};
var listargpe = function (id) {
    var table = $('#grupp').DataTable({
        "destroy": true,
        "autoFill": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/grupopeque.php?opcion=con',
            'data': {id: id}
        },
        "columns": [
            {'data': 'nombregp'},
            {'data': 'escuela'},
            {'data': 'carrera'},
            {'data': 'lider'}

        ],
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "columnDefs": [
            {
                targets: [0, 1, 2],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],
        "language": idioma_espanol
    });
};
var listargp = function (id) {
    var table = $('#selecgrupope').DataTable({
        "destroy": true,
        "autoFill": true,
        "processing": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/grupopeque.php?opcion=grupope',
            'data': {id: id}
        },
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "autoWidth": true,
        "columns": [
            {'data': 'nombre'},
            {'data': 'boton'}
        ],
        responsive: true,
        "columnDefs": [
            {
                "data": null,
                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });
};
var listardatainte = function (id) {
    var table = $('#tableintegrante').DataTable({
        "destroy": true,
        "autoFill": true,
        "processing": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/grupopeque.php?opcion=cofiglid',
            'data': {id: id}
        },
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "autoWidth": true,
        "columns": [
            {'data': 'integrante'},
            {'data': 'cargo'},
            {'defaultContent': '<center><button type="button" class="btn btn-outline-primary btn-sm waves-effect editar" data-toggle="modal" data-target="#ModalcargoInte" title="MODIFICAR CARGO" style="width: 20px;"><i class="fa fa-pencil-square-o " style="margin-left: -7px;" ></i></button>	<button type="button" class="btn btn-outline-danger btn-sm  waves-effect Eliminar"  style="width: 20px; "><center><i class="fa fa-trash-o" style="margin-left: -7px;"></i></center></button></center>'}
        ],
        responsive: true,
        "columnDefs": [

            {

                "data": null,
                "title": "<center>ACCIONES</center>", "targets": 0,
                "title": "<center>ACCIONES</center>", "targets": 0,
                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });
    obtener_data_seleccionar_cargo("#tableintegrante tbody", table);
    obtener_data_eliminar_integrante("#tableintegrante tbody", table);
};
var tableevaluar = function (id) {
    var table = $('#tableevaluar').DataTable({
        "destroy": true,
        "autoFill": true,
        "processing": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/grupopeque.php?opcion=tabeva',
            'data': {id: id}
        },
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "autoWidth": true,
        "order": [],
        "columns": [
            {'data': 'nombre'},
            {'data': 'boton'}
        ],
        responsive: true,
        "columnDefs": [

            {
                "data": null,
                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });
    obtener_data_evaluar_persona("#tableevaluar tbody", table);
};
var tablesepersona = function (id) {
    var table = $('#tablesepersona').DataTable({
        "destroy": true,
        "autoFill": true,
        "processing": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/grupopeque.php?opcion=perso',
            'data': {id: id}
        },
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "autoWidth": true,
        "columns": [
            {'data': 'foto'},
            {'data': 'persona'},
            {'defaultContent': '<center><button type="button" id="idpersonaagr" class="btn btn-outline-primary btn-sm waves-effect " title="AGREGAR PERSONA" ><i class="fa fa-pencil-square-o "  ></i>SELECCIONAR</button></center>'}
        ],
        responsive: true,
        "columnDefs": [

            {

                "data": null,
                "title": "<center>ACCIONES</center>", "targets": 0,
                "title": "<center>ACCIONES</center>", "targets": 0,
                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });
    obtener_data_seleccionar_persona("#tablesepersona tbody", table);
};

var obtener_data_eliminar_integrante = function (tbody, table) {
    $(tbody).on("click", "button.Eliminar", function () {
        var data = table.row($(this).parents('tr')).data();
        console.log(data);
        var cargo = data.id_integrante_cargo;
        var registro = data.id_registro;
        var u = $(".x_panel").data("numero");

        swal({
            title: 'Estás seguro?',
            text: "No podrás revertir esto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SI,ELIMINAR!',
            cancelButtonText: 'NO, CANCELAR!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            $.ajax({
                type: 'POST',
                async: false,
                url: "ajax/datosgrup.php?opcion=DEL",
                data: {cargo: cargo, registro: registro},
                success: function (data) {
                    var dato = JSON.parse(data);
                    if (dato[0].alert === "success") {
                        swal(
                                '',
                                dato[0].mensaje + '¡',
                                dato[0].alert
                                );
                        listardatainte(u);
                    }
                    if (dato[0].alert === "warning") {
                        swal(
                                '',
                                dato[0].mensaje + '¡',
                                dato[0].alert
                                );
                        return false;
                    }
                }
            });
        });

    });
};
var obtener_data_seleccionar_cargo = function (tbody, table) {
    $(tbody).on("click", "button.editar", function () {
        var data = table.row($(this).parents('tr')).data();
        console.log(data);
        $('#formcargo #cargos').val(data.id_cargo_gp);
        $('#formcargo #integrante_cargo').val(data.id_integrante_cargo);
        $('#formcargo #registro').val(data.id_registro);

    });
};

var obtener_data_seleccionar_persona = function (tbody, table) {
    $(tbody).on("click", "button#idpersonaagr", function () {
        var data = table.row($(this).parents('tr')).data();
        var idperiodo = $('#grupoperi').val();
        var idpersona = data.id_persona;
        var u = $(".x_panel").data("numero");
        $.ajax({
            type: 'POST',
            url: "ajax/datosgrup.php?opcion=ADDDAGRUP",
            async: false,
            data: {'idgruper': idperiodo, "idpersona": idpersona},
            error: function () {
                swal(
                        'ERROR!',
                        'PAGINA NO ENCONTRADA',
                        'error'
                        );
            }, success: function (data) {
//                  swal('¡OK!', data, 'success');
                if (data === "OK") {

                    swal('¡OK!', 'Se han guardado los cambios correctamente!', 'success');
                    tablesepersona(u);
                    listardatainte(u);
                }
                if (data === "NO") {
                    swal(
                            'ERROR!',
                            'AL EFECCTUAR LA ACCION',
                            'error'
                            );
                }

            }

        });
    });
};

var obtener_data_evaluar_persona = function (tbody, table) {
    $(tbody).on("click", "button#evaluar", function () {
        var data = table.row($(this).parents('tr')).data();

        var idpersona = data.id;
//        var igles = $('#idiglesiaperso').data('numero');
        var igles = 1;
        var id_registro = data.id_registro;
        var query = $("#nose").data("dato");

        $.ajax({
            type: 'POST',
            url: "ajax/datosgrup.php?opcion=VE",
            success: function (data) {
                var dato = JSON.parse(data);

                if (dato[0].alert === "success") {
                    $('.ValidarEbalu').removeAttr("disable");
                    $('.ValidarEbalu').attr("data-target", "#modalevaluar");

                    var datos = $.ajax({
                        type: 'POST',
                        async: false,
                        url: "ajax/grupopeque.php?opcion=personaiglesia",
                        dataType: 'text',
                        data: {idpersona: idpersona, idiglesia: igles, idregistro: id_registro, query: query}
                    }).responseText;
                    document.getElementById("mostrarpersonaitemevalu").innerHTML = datos;
                    console.log(data);
                }
                if (dato[0].alert === "warning") {

                    swal(
                            '',
                            dato[0].mensaje + '¡',
                            dato[0].alert
                            );
                    $('.ValidarEbalu').attr("disable", "disable");
                    $('.ValidarEbalu').removeAttr("data-target", "#modalevaluar");
                    return false;
                }
            }
        });







    });
};

function fetch_post_data(post_id)
{
    var igles = 1;
    var id_registro = $('#id_estudiante').val();
    var query = $("#nose").data("dato");
    $.ajax({
        url: "ajax/grupopeque.php?opcion=personaiglesia",
        method: "POST",
        data: {idpersona: post_id, idiglesia: igles, idregistro: id_registro, query: query},
        success: function (data)
        {
            $('#mostrarpersonaitemevalu').html(data);
        }
    });
}

function MpAdd() {
    var fecha = new Date();
    var ano = fecha.getFullYear();
    $.ajax({
        type: 'POST',
        url: "ajax/grupopeque.php?opcion=MostraPeriAdd",
        data: {anio: ano},
        async: false,
        success: function (data) {
            var datos = JSON.parse(data);
            var c = datos["data"];
//            console.log(c[0].A);

            for (var i = 0; i < c.length; i++) {
                $("#periodoAdd").append(c[i].C);
            }

        }
    });
}


function AP() {
    var fecha = new Date();
    var ano = fecha.getFullYear();
    $.ajax({
        type: 'POST',
        url: "ajax/grupopeque.php?opcion=añosPeriodos",
        data: {anio: ano},
        async: false,
        success: function (data) {
            var datos = JSON.parse(data);
            var c = datos["data"];
//            console.log(c[0].A);

            for (var i = 0; i < c.length; i++) {
                $("#añoPeriodo").append(c[i].C);
            }
//            tablesemana();
            tablesemana(c[0].A);
        }
    });
}

var tablesemana = function (id) {
    var table = $('#TBSemana').DataTable({
        "destroy": true,
        "autoFill": true,
        "processing": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/grupopeque.php?opcion=Semana',
            'data': {id: id}
        },
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "autoWidth": true,
        "order": [],
        "columns": [
            {'data': 'semana'},
            {'data': 'leccion'},
            {'data': 'fecha_ini_form'},
            {'data': 'fecha_fin_form'},
            {'data': 'boton'}
        ],
        responsive: true,
        "columnDefs": [

            {
                "data": null,
                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });

};