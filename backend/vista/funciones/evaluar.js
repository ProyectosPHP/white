$(document).ready(function () {




    $('#evaluarform').on('submit', function (e) {
        e.preventDefault();
        var idchekbox = [];
        var valorchekbox = [];
        var id_integrante = $('#id_estudiante').val();
        $("input[type=checkbox]:checked").each(function (i) {
            idchekbox[i] = $(this).data('numero');
            valorchekbox[i] = $(this).val();
            
        });
        idchekbox.push($("input[type=number]").data('numero'));
        valorchekbox.push($("input[type=number]").val());
             
          $.ajax({
            url: 'controlador/Validar.php',
            method: 'POST',
            data: {idchekbox: idchekbox, valorchekbox: valorchekbox, id_estudiante: id_integrante},
            success: function (data)
            {
//                console.log(data);
                if (data === "OK") {
                    swal('¡OK!', 'SE GUARDARON LOS DATOS CORRECTAMENTE!', 'success');
                }
                if (data === "NO") {
                    swal('¡ERROR!', 'ERROR AL GUARDAR LOS DATOS!', 'error');
                }
            }, error: function () {
                swal(
                        'ERROR!',
                        'PAGINA NO ENCONTRADA',
                        'error'
                        );
            }
        });

        return  false;
    });
});
