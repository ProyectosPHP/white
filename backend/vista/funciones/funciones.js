$(document).ready(function () {
 
    $('#creargrupe').on("submit", function (e) {
        e.preventDefault();
        var nomgrupe = $("#nomgrupe").val();
        var iglesias = $("#iglesias").val();
        if (nomgrupe !== '' && iglesias !== null) {
            $.ajax({
                type: 'POST',
                url: "controlador/ControlAgregrup.php?opcion=addgrupp",
                async: true,
                cache: false,
                data: {'nomgrupe': nomgrupe, 'iglesias': iglesias},
                error: function () {
                    swal(
                            'ERROR!',
                            'PAGINA NO ENCONTRADA',
                            'error'
                            );
                }, success: function (data) {
                    if (data === "OK") {
                        swal(
                                'OK!',
                                'EL GRUPO SE AGREGO CORRECTAMENTE',
                                'success'
                                );
                        $("#nomgrupe").val('');
                        $("#iglesias").val('');
                    }
                    if (data === "NO") {
                        swal(
                                'ERROR!',
                                'AL AGREGAR EL GRUPO',
                                'error'
                                );
                        $("#nomgrupe").val('');
                        $("#iglesias").val(null);
                    }
                    if (data === "LLENE") {
                        swal(
                                'ERROR!',
                                'LLENE TODOS LOS DATOS',
                                'warning'
                                );
                        $("#nomgrupe").val('');
                        $("#iglesias").val(null);
                    }
                }
            });
        } else {
            swal('ERROR!',
                    'LLENE TODOS LOS DATOS',
                    'warning'
                    );
        }


    });
    $('#agregaper').click(function (e) {
        e.preventDefault();
        var periodo = $("#idgru").data("numero");
        $('#grupoperi').val(periodo);
    });
    $('#btncreargp').click(function (e) {
        e.preventDefault();
        var nomgrupe = $('#nomgrupe').val();
        var lider = $('#lider').val();
        var carrera = $('#carrera').val();
        if (nomgrupe !== '' && lider !== null && carrera !== null) {
            $.ajax({
                type: 'POST',
                url: 'ajax/escuelagrupo.php?opcion=ADDESCU',
                async: false,
                data: {'nomgrupe': nomgrupe, 'carrera': carrera, 'lider': lider},
                error: function () {
                    swal(
                            'ERROR!',
                            'PAGINA NO ENCONTRADA',
                            'error'
                            );
                }, success: function (data) {
                    var dato = JSON.parse(data);
                    if (dato[0].alert === "success") {
                        swal(
                                '',
                                dato[0].mensaje + '¡',
                                dato[0].alert
                                );

                      
                    }
                    if (dato[0].alert === "warning") {
                        swal(
                                '',
                                dato[0].mensaje + '¡',
                                dato[0].alert
                                );
                        return false;
                    }

                }
            });


        } else {
            swal(
                    'ERROR!',
                    'LLENE TODO LOS DATOS',
                    'warning'
                    );
        }
        return false;
    });
});

