$(document).ready(function () {
    TbManUsers();
});
var idioma_espanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ning&uacuten dato disponible en esta tabla ,por favor introducir datos",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

var TbManUsers = function () {
    var table = $('#TbUsuarios').DataTable({
        "destroy": true,
        "autoFill": true,
        "ajax": {
            'method': 'POST',
            'url': 'ajax/Usuarios.php?opcion=RepUs'
        },
        "order": [],
        "columns": [
            {'data': 'login'},
            {'data': 'persona'},
            {'data': 'boton'}
        ],
        "lengthMenu": [5, 10, 25, 50, 75, 100],
        "columnDefs": [
            {
                "data": null,

                "defaultContent": "",
                "width": "20%", "targets": 0,
                "targets": -1
            }
        ],
        "language": idioma_espanol
    });

};