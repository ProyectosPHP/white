<?php
session_start();
if (!$_SESSION["validar"] && !$_SESSION["usuario"]["id_rol"]) {
    header("location:../index.php?action=logeo");

    exit();
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#0275d8" />
        <title>PRINCIPAL</title>
        <link href='vista/recursos/images/logo.png' rel='shortcut icon' type='image/png'> 
        <link rel="stylesheet" href="vista/css/bootstrap.min.css">
        <link rel="stylesheet" href="vista/css/datepicker.css">
        <link rel="stylesheet" href="vista/css/font-awesome.min.css">
        <link rel="stylesheet" href="vista/css/jquery.dataTables.min.css">
        <!--<link rel="stylesheet" href="vista/css/dataTables.bootstrap.min.css">-->
        <link rel="stylesheet" href="vista/css/alertify.bootstrap.css" id="toggleCSS" />
        <link rel="stylesheet" href="vista/css/alertify.default.css" id="toggleCSS" />
        <link rel="stylesheet" href="vista/css/alertify.core.css" />
        <link rel="stylesheet" href="vista/css/dataTables.material.min.css" />
        <link rel="stylesheet" href="vista/css/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="vista/css/sweetalert2.css" id="toggleCSS" />
        <link rel="stylesheet" href="vista/css/bootstrap-chosen.css" />
        <link rel="stylesheet" href="vista/css/icon.css" />
        <link rel="stylesheet" href="vista/css/custom.min.css" />
        <link rel="stylesheet" href="vista/css/nprogress.css" />
        <link rel="stylesheet" href="vista/css/green.css" />
        <link rel="stylesheet" href="vista/css/bootstrap-progressbar-3.3.4.min.css" />
        <link rel="stylesheet" href="vista/css/jqvmap.min.css" />
        <link href="vista/css/mdb2.min.css" rel="stylesheet">
        <link href="vista/css/prism.min.css" rel="stylesheet">
        <link href="vista/css/style.min.css" rel="stylesheet">
        <link rel="stylesheet" href="vista/css/daterangepicker.css" />
        <script src="vista/js/jquery-3.2.1.min.js"></script>
        
        <script type="text/javascript" src="vista/funciones/grupo.js"></script>
        <script type="text/javascript" src="vista/js/sweetalert2.min.js"></script>
        <script type="text/javascript" src="vista/funciones/funciones.js"></script>
        <script type="text/javascript" src="vista/funciones/salir.js"></script>
        <script type="text/javascript" src="vista/funciones/tablas.js"></script>
        <script type="text/javascript" src="vista/funciones/dat.js"></script>
        <script type="text/javascript" src="vista/funciones/esc.js"></script>
        <script type="text/javascript" src="vista/funciones/evaluar.js"></script>
        <script type="text/javascript" src="vista/funciones/mostrar.js"></script>
        <script src="vista/js/alertify.min.js"></script>
        <script src="vista/js/jquery.min.js"></script>
        <script src="vista/js/chosen.jquery.js"></script>
    </head>
    <body class="nav-md " style="background:#00acc1;">
        <style>
            ul#co,li#co,a#co{
                background:#ededed;
                color: #000000;
            }
            #lin,a#co{
                color: #000000;
            }
            #line{
                color: #000000;
            }
            a#line:hover{
                color: #000000;
            }
            ul#co,li#co,a#co:hover{
                background:#ededed;
                color: #000000;
            }
            .nav.side-menu>li>a:hover {
                color: #000 !important;
            }
            .nav.side-menu>li.current-page, .nav.side-menu>li.active {
                border-right: 5px solid #0275d8;
            }
            .nav-sm ul.nav.child_menu {
                background: #ededed;
            }
            .nav.navbar-nav>li>a {
                color: #fff !important;
            }
            .nav-sm .nav.child_menu li.active, .nav-sm .nav.side-menu li.active-sm {
                border-right: 5px solid #0275d8;
            }
            @media (min-width: 992px){
                footer {
                    margin-left: 0px;
                }
            }

            .chosen-container-single .chosen-search input[type="text"] {
                height: 17px;       
                color: #0275d8;
                width: 92%; 
            }
            div#grupp_length{
                margin-top: 15px;
                text-align: left;
            }
            select[name*="grupp_length"]{
                width: 45px;
            }
            .fa-chevron-down:before {

                color: #000;
            }
        </style>
        <?php
        require 'modelos/Modulosmodelo.php';

        $id_rol = $_SESSION["usuario"]["id_rol"];
        ?>
        <div class="container body" style=" background:#ededed;">
            <div class="main_container" style="background:#ededed;">
                <div class="col-md-3 left_col menu_fixed mCustomScrollbar _mCS_1 mCS-autoHide" style="background:#ededed;">
                    <div class="left_col scroll-view" style="background:#ededed;">
                        <div class="navbar nav_title" style=" background:#ededed; border: 0;">
                            <a href="menu.jsp" class="site_title">
                                <i class="fa fa-university" id="lin">
                                </i> <span id="lin" >WHITE</span></a>
                        </div>
                        <div class="clearfix"></div>

                        <div class="profile">
                            <div class="profile_pic">
                                <img src="vista/recursos/images/perfil.png" alt="..." class="img-circle profile_img animated bounceInDown">
                            </div>
                            <div class="profile_info">
                                <span id="lin">Bienvenido</span>
                                <h2 id="lin"><?php echo substr($_SESSION["usuario"]["usuario"], 0, 11); ?></h2>
                            </div>
                        </div>
                        <br />
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3 id="lin">General</h3>
                                <?php
                                require 'modelos/RolModelo.php';
                                $modulos = ModeloPermisos($id_rol);
                                ?>
                                <ul class="nav side-menu">
                                    <li>
                                        <a id="co" href="index.php" id="line"><i class="fa fa-home" id="lin" aria-hidden="true"></i>INICIO</a>
                                    </li>
                                    <?php
                                    foreach ($modulos as $modulos) {
                                        if ($modulos["id_modulo"] <= 6 && $modulos["estado"] == "true") {
                                            ?>
                                            <li>
                                                <a id="co" class="text-black" href="<?php
                                                if ($modulos["enlace"] != "null") {
                                                    echo $modulos["enlace"];
                                                }if ($modulos["enlace"] == null) {
                                                    echo '#';
                                                }
                                                ?>"><i id="lin" class="<?php echo $modulos["imagen"] ?>" aria-hidden="true"></i><?php echo $modulos["nombre"]; ?> <?php echo $modulos["span"]; ?></a>
                                                   <?php
                                                   $submmodulos = ModeloUserPermisos($id_rol, $modulos["id_modulo"]);
                                                   $submmodulo = ModeloUserIdPermisos($id_rol, $modulos["id_modulo"]);

                                                   if ($submmodulo["id_modulo_sup"] != 0) {
                                                       ?>
                                                    <ul class="nav child_menu">
                                                        <?php
                                                        foreach ($submmodulos as $submmodulos) {
                                                            if ($submmodulos["estado"] == "true") {
                                                                ?>
                                                                <li><a id="lin" href="<?php
                                                                    if ($submmodulos["enlace"] != "null") {
                                                                        echo $submmodulos["enlace"];
                                                                    }if ($submmodulos["enlace"] == null) {
                                                                        echo '#';
                                                                    }
                                                                    ?>"><?php echo $submmodulos["descripcion"] ?></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                    </ul>
                                                    <?php
                                                }
                                            }
                                            ?> </li>
                                        <?php
                                    }
                                    ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu" style="color: #fff;background: #0275d8;">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars" style="color: #fff;"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">

                                <li class="" style="background: #0275d8;color: #fff;">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="background: #0275d8;color: #fff;">
                                        <img src="vista/recursos/images/perfil.png" class="animated bounceInDown" alt=""><?php echo substr($_SESSION["usuario"]["usuario"], 0, 11); ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li style="background: #0275d8;color: #fff;"><a href="javascript:;" style="background: #0275d8;color: #fff;"> PERFIL</a></li>
                                        <li style="background: #0275d8;color: #fff;">
                                            <a href="" style="background: #0275d8;color: #fff;"> 
                                                <span class="badge bg-red pull-right">50%</span>
                                                <span><span>CONFIGURACI&Oacute;N</span>
                                            </a>
                                        </li>
                                        <li style="background: #0275d8;color: #fff;"><a href="" id="btnsalir" style="background: #0275d8;color: #fff;"><i class="fa fa-sign-out pull-right"></i>CERRAR SESION</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="right_col" role="main"  style="min-height: 350px; background: #FFFFFF;" >
                    <?php
                    require 'modelos/GpPequemodelos.php';
                    $modulos = new enlaces();
                    $modulos->enlacesController();
                    ?>
                </div>
                <!--                <footer>
                                    <div class="pull-right">
                                        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </footer>-->
                <footer>
                    <div class="pull-right">
                        Ingenieria de Sistemas <a href="">VI</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>

        </div>

        <script type="text/javascript" src="vista/js/tether.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="vista/js/mdb.min.js"></script>
        <!-- Prism -->
        <script type="text/javascript" src="vista/js/prism.min.js"></script>
        <!--<script type="text/javascript" src="vista/js/jquery-3.1.1.min.js"></script>-->
        <script src="vista/js/jquery-2.2.2.min.js"></script>
        <script src="vista/js/bootstrap.min.js"></script>
        <script src="vista/js/sweetalert2.min.js"></script>
        <script src="vista/js/dataTables.material.min.js"></script>
        <script src="vista/js/dataTables.bootstrap4.min.js"></script>

        <script src="vista/js/jquery.dataTables.min.js"></script>
        <script src="vista/js/bootstrap-datepicker.js"></script>
        <script src="vista/js/custom.min.js"></script>
        <script type="text/javascript" src="vista/js/materialize.min.js"></script>
    </body>
</html>