<?php

require_once '../modelos/escuela.php';
require_once '../modelos/GpPequemodelos.php';

$periodo = ModeloPeriodo();
$fecha = new DateTime();
$anio = $fecha->format('Y');
$mes = $fecha->format('m');
if ($periodo["nombre"] == $anio) {
    if ($mes <= 6) {

        $ciclo = $periodo["nombre"] . "-I";
    }if ($mes > 6) {
        $ciclo = $periodo["nombre"] . "-II";
    }
    $periodos = $periodo["id_periodo"];
}
$DatosController = array(
    "id_periodo" => $periodos,
    "id_escuela_sabatica" => $_POST["id_escuela_sabatica"],
    "id_grupo_pequeno" => $_POST["id_grupo_pequeno"],
    "ciclo_periodo" => $ciclo);
$agregar = AgregarEscuelaGrupo($DatosController);
if ($agregar == "success") {
    echo 'SI';
} else {
    echo 'NO';
}
?>