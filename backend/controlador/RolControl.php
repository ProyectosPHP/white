<?php
require '../modelos/RolModelo.php';
$modulos = ModeloPermisos("1");
?>
<ul class="nav side-menu">
            <li>
                <a id="co" href="#"><i class="fa fa-home" aria-hidden="true"></i>INICIO</a>
            </li>
<?php 
foreach ($modulos as $modulos) {
    if ($modulos["id_modulo"] <= 6 && $modulos["estado"] == "true") {
 ?>
 <li>
     <a id="co" href="#"><i class="<?php echo $modulos["imagen"]?>" aria-hidden="true"></i><?php echo $modulos["nombre"];?> <?php echo $modulos["enlace"];?></a>
 <?php 
 $submmodulos = ModeloUserPermisos("1", $modulos["id_modulo"]);
 $submmodulo = ModeloUserIdPermisos("1", $modulos["id_modulo"]);
 echo $submmodulo["id_modulo_sup"];
 if($submmodulo["id_modulo_sup"]!=0){
    ?>
 <ul class="nav child_menu">
     <?php
    foreach ($submmodulos as $submmodulos) {
        if ($submmodulos["estado"] == "true") {
            ?>
<li><a href="#"><?php echo $submmodulos["descripcion"]?></a></li>
<?php
        }
        
    }
    ?>
</ul>
 <?php
}
    }
?> </li>
 <?php
    }
?>
  </ul>
