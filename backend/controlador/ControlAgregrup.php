<?php

require_once '../modelos/GpPequeModeloAgr.php';

if (isset($_GET["opcion"])) {
    if ($_GET["opcion"] == "addgrupp") {
        if ($_POST["iglesias"] != null && $_POST["nomgrupe"] != '') {
            $DatosModel = array("iglesias" => $_POST["iglesias"],
                "nomgrupe" => $_POST["nomgrupe"]);
            $respuesta = AgregarGrupo($DatosModel);
            if ($respuesta == "success") {
                echo 'OK';
            } else {
                echo 'NO';
            }
        } else {
            echo 'LLENE';
        }
    }
    if ($_GET["opcion"]=="moddatgrup") {
        if ($_POST["blanco_baut"]=='') {
        $DatosModel = array("id_grupo_periodo" => $_POST["id_grupo_periodo"],
            "lema" => $_POST["lema"],
            "canto" => $_POST["canto"],
            "versiculo" => $_POST["versiculo"],
            "blanco_baut" => 'null');    
        } else {
        $DatosModel = array("id_grupo_periodo" => $_POST["id_grupo_periodo"],
            "lema" => $_POST["lema"],
            "canto" => $_POST["canto"],
            "versiculo" => $_POST["versiculo"],
            "blanco_baut" => $_POST["blanco_baut"]);    
        }
        

        
        $respuesta = ActualizarDatos($DatosModel);
        if ($respuesta == "success") {
            echo 'OK';
        } else {
            echo 'NO';
        }
    }
}