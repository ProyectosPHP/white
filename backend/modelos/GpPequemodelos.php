<?php

require_once 'conexion.php';

function ModeloCarrera() {
    $stmt = Conexion::conectar()->prepare("SELECT id_carrera, carrera, abreviatura, descripcion, estado,id_facultad,facultad FROM fn_listarcarrera()");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloCarreraFacultad() {
    $stmt = Conexion::conectar()->prepare("SELECT id_univ_unidacad, nombre, abreviatura, descripcion, id_univ_unidacad_sup, estado FROM public.univ_unidacad WHERE id_univ_unidacad<=3");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloAlumnoUni($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT id_pers, alert_pers, mensaje_pers FROM fn_crud_persona_universidad (null,null,'" . $DatosModel["codigo"] . "','" . $DatosModel["nombres"] . "','" . $DatosModel["apepat"] . "' ,'" . $DatosModel["apemat"] . "' ," . $DatosModel["id_tipo_doc"] . ",'" . $DatosModel["nro_doc"] . "'," . $DatosModel["id_carrera"] . "," . $DatosModel["id_ciclo"] . ",null,'" . $DatosModel["fecha_nac"] . "','" . $DatosModel["telefono"] . "','" . $DatosModel["correo"] . "','" . $DatosModel["sexo"] . "','" . $DatosModel["direccion"] . "','" . $DatosModel["foto"] . "', '', '1', '1')");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloCargogrupo() {
    $stmt = Conexion::conectar()->prepare("SELECT id_cargo,cargo from fn_cargogrupo()");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloValidarCargogrupo($idusu, $idcar) {
    $stmt = Conexion::conectar()->prepare("SELECT COUNT(ic.id_cargo_gp) as nro, ic.id_cargo_gp as id_argo FROM  persona p, registro r LEFT JOIN integrante_cargo ic ON r.id_registro = ic.id_registro LEFT JOIN cargo_gp c ON c.id_cargo_gp = ic.id_cargo_gp
WHERE p.id_persona = r.id_integrante AND r.id_grupo_periodo =$idusu AND ic.id_cargo_gp=$idcar group by ic.id_cargo_gp;");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloModulperfiles($id) {
    $stmt = Conexion::conectar()->prepare("SELECT  m.id_modulo,m.nombre
FROM public.modulo m
WHERE m.id_modulo NOT IN (SELECT (id_modulo) FROM rol_opcion WHERE id_rol = $id);
");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloModulperfilId($id) {
    $stmt = Conexion::conectar()->prepare("SELECT ro.id_rol_opcion,ro.id_rol,ro.id_modulo,m.nombre,m.descripcion
FROM public.modulo m, public.rol r, public.rol_opcion ro
WHERE m.id_modulo = ro.id_modulo AND r.id_rol = ro.id_rol AND r.id_rol=$id order by ro,id_modulo asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloDatoevaluar() {
    $stmt = Conexion::conectar()->prepare("SELECT r.id_registro, r.id_grupo_periodo, gpe.id_grupo_pequeno, gp.nombre as nombregp,p.id_persona, CONCAT(p.nombres,' ',p.apepat,' ',p.apemat) integrante
FROM public.registro r, public.grupo_periodo gpe, public.persona p, public.grupo_pequeno gp
WHERE gpe.id_grupo_periodo = r.id_grupo_periodo AND p.id_persona = r.id_integrante AND gp.id_grupo_pequeno = gpe.id_grupo_pequeno;
");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloDatoevaluarId($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_registro, id_integrante, UPPER(integrante) as integrante, id_integrante_cargo, id_cargo_gp, cargo 
	FROM fn_list_integrantes_cargo_gp_2($id) order by id_integrante asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloUniversida() {
    $stmt = Conexion::conectar()->prepare("SELECT id_universidad, nombre, abreviatura, estado FROM public.universidad ORDER BY id_universidad asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloCiclo() {
    $stmt = Conexion::conectar()->prepare("SELECT id_ciclo, nombre, nro, nro_romanos, estado  FROM public.ciclo order by id_ciclo asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloPeriodo() {
    $stmt = Conexion::conectar()->prepare("SELECT id_periodo, nombre, estado FROM public.periodo order by id_periodo asc");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloFilial() {
    $stmt = Conexion::conectar()->prepare("SELECT id_periodo, nombre, estado FROM public.periodo");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloTipodoc() {
    $stmt = Conexion::conectar()->prepare("SELECT id_tipo_doc, nombre, abreviatura FROM public.tipo_doc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModelotablaPersona($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_persona,persona,foto FROM public.fn_list_personas_sin_gp_2($id)");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloPersona() {
    $stmt = Conexion::conectar()->prepare("SELECT id_persona, nombres, apepat, apemat, id_tipo_doc, nro_doc, id_ubigeo,fecha_nac, telefono, correo, sexo, direccion, foto, descripcion, 
       fecha_reg FROM public.persona order by id_persona asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloGrupoPequeno() {
    $stmt = Conexion::conectar()->prepare("SELECT id_grupo_pequeno, id_iglesia, nombre as nombregpo, fecha_reg, estado FROM public.grupo_pequeno order by nombre asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloIglesia() {
    $stmt = Conexion::conectar()->prepare("SELECT id_iglesia, nombre, descripcion estado FROM public.iglesia");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloSabeCarrera($id) {
    $stmt = Conexion::conectar()->prepare("SELECT   gpe.id_carrera_gp
FROM escuela_sabatica es, grupo_pequeno gp, reg_gp_escuela_sabatica res, univ_unidacad c, grupo_periodo gpe
  LEFT  JOIN persona p  ON p.id_persona = gpe.id_lider
WHERE es.id_escuela_sabatica = res.id_escuela_sabatica AND gpe.id_grupo_periodo = res.id_grupo_periodo AND
  gp.id_grupo_pequeno = gpe.id_grupo_pequeno  AND c.id_univ_unidacad = gpe.id_carrera_gp AND  gpe.id_lider=$id limit 1");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloEscuelaSabatica() {
    $stmt = Conexion::conectar()->prepare("SELECT es.id_escuela_sabatica, es.nombre AS escuela_sabatica, es.id_periodo, es.fecha_reg AS fecha_reg_es, p.nombre AS periodo, 
  es.id_unidadacad_univ,uu.id_univ_unidacad,uuni.nombre AS univ_unidacad,uuni.abreviatura AS abv_univ_unidacad,uuni.descripcion AS descrip_univ_unidacad, 
  uuni.id_univ_unidacad_sup,uu.id_universidad,u.nombre AS universidad,u.abreviatura AS abv_universidad,uu.distrito_misionero,uu.abreviatura AS abv_unidadacad_univ, 
  uu.descripcion AS descri_unidadacad_univ,uu.estado AS estado_unidadacad_univ,uu.fecha_reg AS fecha_reg_unidadacad_univ
FROM public.escuela_sabatica es,public.periodo p,public.unidadacad_univ uu,public.univ_unidacad uuni,public.universidad u
WHERE p.id_periodo = es.id_periodo AND uu.id_unidadacad_univ = es.id_unidadacad_univ AND uuni.id_univ_unidacad = uu.id_univ_unidacad AND u.id_universidad = uu.id_universidad
 order by es.nombre asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloGrupoPeque($id) {
    $stmt = Conexion::conectar()->prepare("SELECT gpe.id_grupo_periodo, gp.id_grupo_pequeno, gp.nombre, gpe.id_lider
FROM grupo_pequeno gp, grupo_periodo gpe WHERE gp.id_grupo_pequeno = gpe.id_grupo_pequeno 
  AND gpe.id_grupo_periodo NOT IN (SELECT id_grupo_periodo FROM reg_gp_escuela_sabatica WHERE id_escuela_sabatica = $id) 
  AND gpe.id_grupo_periodo NOT IN (SELECT id_grupo_periodo FROM reg_gp_escuela_sabatica WHERE id_escuela_sabatica <> $id)");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ModeloAñoPeriodo() {
    $stmt = Conexion::conectar()->prepare("SELECT id_periodo, nombre, estado FROM periodo");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ValidarIngresoSemana($periodo,$fechaini,$fechafin) {
    $stmt = Conexion::conectar()->prepare("SELECT COUNT(id_semana) va FROM public.semana  WHERE id_periodo='$periodo'  AND  date('$fechaini')  BETWEEN fecha_ini AND fecha_fin OR date('$fechafin')  BETWEEN fecha_ini AND fecha_fin ");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}



function ModeloTablSEMANA($anio) {
    $stmt = Conexion::conectar()->prepare("SELECT s.id_semana,p.nombre as anio,s.nombre as semana,s.descripcion as  leccion, s.fecha_ini,s.fecha_fin
FROM public.semana s, public.periodo p
WHERE p.id_periodo = s.id_periodo AND p.nombre='$anio' ");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloGrupoPeriodo() {
    $stmt = Conexion::conectar()->prepare("SELECT gp.id_grupo_periodo, p.nombre as periodo, gp.id_grupo_pequeno, gpe.nombre as nombregp, gp.id_carrera_gp, uu.nombre as carrera, gp.id_lider, concat(pe.nombres, ' ' ,pe.apepat, ' ',pe.apemat)as lider
FROM public.grupo_periodo gp,public.grupo_pequeno gpe,public.periodo p, public.univ_unidacad uu, public.persona pe
WHERE  gpe.id_grupo_pequeno = gp.id_grupo_pequeno AND  p.id_periodo = gp.id_periodo AND  uu.id_univ_unidacad = gp.id_carrera_gp AND  pe.id_persona = gp.id_lider");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function Modeloidgrupperio($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_grupo_periodo,lema, canto, versiculo, blanco_baut,id_lider FROM public.grupo_periodo WHERE id_grupo_periodo=$id");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloidPersonaiglesia($id) {
    $stmt = Conexion::conectar()->prepare("SELECT pi.id_persona_iglesia, pi.id_iglesia, pi.id_persona
FROM public.persona_iglesia pi WHERE pi.id_persona=$id");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloidPersonav2($id) {
    $stmt = Conexion::conectar()->prepare("SELECT  UPPER(nombres) as nombres, UPPER(apepat)as apepat,UPPER(apemat) as apemat FROM public.persona WHERE id_persona=$id");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloidPersonav2antes($id,$id_grupo_periodo) {
    $stmt = Conexion::conectar()->prepare("SELECT id_integrante as id_persona FROM fn_list_integrantes_cargo_gp_2($id_grupo_periodo)  WHERE id_integrante<$id ORDER BY id_integrante DESC LIMIT 1");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloidPersonav2next($id,$id_grupo_periodo) {
    $stmt = Conexion::conectar()->prepare("SELECT id_integrante as id_persona FROM fn_list_integrantes_cargo_gp_2($id_grupo_periodo)  WHERE id_integrante>$id ORDER BY id_integrante ASC LIMIT 1");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloidPersona($id) {
    $stmt = Conexion::conectar()->prepare("SELECT   UPPER(nombres) as nombres, UPPER(apepat)as apepat,UPPER(apemat) as apemat FROM public.persona WHERE id_persona=$id order by nombres asc ");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloAdminEsc() {
    $stmt = Conexion::conectar()->prepare("SELECT id_reg_gp_es, id_escuela_sabatica, escuela_sabatica, id_grupo_periodo, grupo_pequeno, id_carrera_gp, carrera_gp, id_lider, lider 
	FROM fn_list_reg_escuela_sabatica()
");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloAdminEscID($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_reg_gp_es, id_escuela_sabatica, escuela_sabatica, id_grupo_periodo, grupo_pequeno, id_lider, lider ,carrera_gp ,id_carrera_gp 
FROM fn_list_reg_escuela_sabatica() WHERE id_escuela_sabatica = $id ORDER BY escuela_sabatica");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloDatogrup($idlider) {
    $stmt = Conexion::conectar()->prepare("SELECT  res.id_reg_gp_es, es.id_escuela_sabatica, gpe.id_grupo_periodo, gp.id_grupo_pequeno, 
  es.nombre as escuela, gp.nombre as nombregp, gpe.id_carrera_gp, gpe.lema, gpe.canto, gpe.versiculo, gpe.blanco_baut as blanco_baut, 
  p.nombres||' '||p.apepat||' '||p.apemat as lider, c.nombre as carrera, gpe.id_lider  
FROM escuela_sabatica es, grupo_pequeno gp, reg_gp_escuela_sabatica res, univ_unidacad c, grupo_periodo gpe 
  LEFT  JOIN persona p  ON p.id_persona = gpe.id_lider, registro r
WHERE es.id_escuela_sabatica = res.id_escuela_sabatica AND gpe.id_grupo_periodo = res.id_grupo_periodo AND
  gp.id_grupo_pequeno = gpe.id_grupo_pequeno  AND c.id_univ_unidacad = gpe.id_carrera_gp AND  r.id_grupo_periodo = gpe.id_grupo_periodo
  AND r.id_integrante =$idlider");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ValidarModeloDatogrup($idlider) {
    $stmt = Conexion::conectar()->prepare("SELECT count(res.id_reg_gp_es) as valor
FROM escuela_sabatica es, grupo_pequeno gp, reg_gp_escuela_sabatica res, univ_unidacad c, grupo_periodo gpe 
  LEFT  JOIN persona p  ON p.id_persona = gpe.id_lider, registro r
WHERE es.id_escuela_sabatica = res.id_escuela_sabatica AND gpe.id_grupo_periodo = res.id_grupo_periodo AND
  gp.id_grupo_pequeno = gpe.id_grupo_pequeno  AND c.id_univ_unidacad = gpe.id_carrera_gp AND  r.id_grupo_periodo = gpe.id_grupo_periodo
  AND r.id_integrante =$idlider");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloAdminRol() {
    $stmt = Conexion::conectar()->prepare("SELECT id_rol, nombre, descripcion, estado FROM public.rol");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloAdminEscc() {
    $stmt = Conexion::conectar()->prepare("SELECT id_escuela_sabatica, nombre FROM public.escuela_sabatica");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloAdminEsccid($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_escuela_sabatica, nombre FROM public.escuela_sabatica WHERE id_escuela_sabatica=$id");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function Configlider($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_registro, id_integrante, integrante, id_integrante_cargo, id_cargo_gp, cargo 
	FROM fn_list_integrantes_cargo_gp_2($id);");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function ModeloAgregarUsuario($DatosModel) {
    try {
        $stmt = Conexion::conectar()->prepare("SELECT  id_usu, alert_usu, mensaje_usu FROM fn_crud_usuario_persona(null,null,null,'" . $DatosModel["codigo"] . "','" . $DatosModel["nombres"] . "','" . $DatosModel["apepat"] . "' ,'" . $DatosModel["apemat"] . "' ," . $DatosModel["id_tipo_doc"] . ",'" . $DatosModel["nro_doc"] . "'," . $DatosModel["id_carrera"] . "," . $DatosModel["id_ciclo"] . ",null,'" . $DatosModel["fecha_nac"] . "','" . $DatosModel["telefono"] . "','" . $DatosModel["correo"] . "','" . $DatosModel["sexo"] . "','" . $DatosModel["direccion"] . "','" . $DatosModel["foto"] . "','','" . $DatosModel["login"] . "','" . $DatosModel["clave"] . "','" . $DatosModel["id_rol"] . "',null,'1','" . $DatosModel["caso"] . "'," . $DatosModel["opcion"] . ")");
        $stmt->execute();
        return $stmt->fetch();
        $stmt->close();
    } catch (Exception $exc) {
        return $exc->getMessage();
    }
}

?>