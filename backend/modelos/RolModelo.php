<?php

require_once 'conexion.php';

function ModeloRol() {
    $stmt = Conexion::conectar()->prepare("SELECT id_rol, nombre, descripcion, estado FROM public.rol order by id_rol asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ModeloUserPermisos($rol,$modulo) {
    $stmt = Conexion::conectar()->prepare("SELECT ro.id_rol_opcion,ro.id_rol,ro.id_modulo,m.nombre,m.descripcion,m.id_modulo_sup,ro.estado,m.enlace
FROM public.rol r,public.rol_opcion ro,public.modulo m
WHERE r.id_rol = ro.id_rol AND m.id_modulo = ro.id_modulo AND r.id_rol=$rol AND m.id_modulo_sup=$modulo order by ro.id_modulo asc
");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ModeloUserIdPermisos($rol,$modulo) {
    $stmt = Conexion::conectar()->prepare("SELECT ro.id_rol_opcion,ro.id_rol,ro.id_modulo,m.nombre,m.descripcion,m.id_modulo_sup,ro.estado
FROM public.rol r,public.rol_opcion ro,public.modulo m
WHERE r.id_rol = ro.id_rol AND m.id_modulo = ro.id_modulo AND r.id_rol=$rol AND m.id_modulo_sup=$modulo order by ro.id_modulo asc
");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}
function ModeloPermisos($id) {
    $stmt = Conexion::conectar()->prepare("SELECT ro.id_rol_opcion,ro.id_rol,r.nombre,m.imagen,m.enlace,m.span,r.descripcion,ro.id_modulo,m.nombre,m.descripcion,m.id_modulo_sup,ro.estado
FROM public.rol r,public.rol_opcion ro,public.modulo m
WHERE r.id_rol = ro.id_rol AND m.id_modulo = ro.id_modulo AND r.id_rol=$id order by ro.id_modulo asc;
");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}


