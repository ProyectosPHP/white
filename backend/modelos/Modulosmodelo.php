<?php
require_once 'conexion.php';

function ModeloModulos() {
    $stmt = Conexion::conectar()->prepare("SELECT id_modulo,nombre, enlace, imagen, descripcion, fecha_reg, id_modulo_sup , estado FROM public.modulo where estado='1' order by id_modulo asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ModeloModulosper($id) {
    $stmt = Conexion::conectar()->prepare("SELECT id_modulo,nombre, enlace, imagen, descripcion, fecha_reg, id_modulo_sup , estado FROM public.modulo where estado='1' order by id_modulo asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ModeloSubModulos($modulo) {
    $stmt = Conexion::conectar()->prepare("SELECT id_modulo,nombre, enlace, imagen, descripcion, fecha_reg, id_modulo_sup , estado FROM public.modulo where estado='1' and id_modulo_sup=$modulo and id_modulo>6 order by id_modulo asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
?>