<?php

require_once 'conexion.php';

function AgregarEscuelaGrupo($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT public.fn_crud_reg_gp_es(null," . $DatosModel["id_periodo"] . "," . $DatosModel["id_escuela_sabatica"] . ",
    " . $DatosModel["id_grupo_pequeno"] . ",'" . $DatosModel["ciclo_periodo"] . "','1','1')");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}
function EditarEstadoModulo($DatosModel) {
    $stmt = Conexion::conectar()->prepare("UPDATE public.rol_opcion SET  estado='" . $DatosModel["permiso"] . "' WHERE id_rol_opcion=" . $DatosModel["id_rol_opcion"] . "");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}
function AddDatosEscuela($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT id, alert, mensaje FROM fn_crud_escuela_sabatica(null, '".$DatosModel["nombre"]."', ".$DatosModel["id_unidadacad_univ"].", 1)");
 $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}
function EditarDatosEscuela($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT id, alert, mensaje FROM fn_crud_escuela_sabatica(".$DatosModel["id_escuela_sabatica"].", '".$DatosModel["nombre"]."', ".$DatosModel["id_unidadacad_univ"].", 2)");
 $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}
function DelDatosEscuela($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT id, alert, mensaje FROM fn_crud_escuela_sabatica(".$DatosModel.", '',null,3)");
 $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}
