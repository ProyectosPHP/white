<?php
require_once 'conexion.php';
function ModeloMantEscuela() {
    $stmt = Conexion::conectar()->prepare("SELECT es.id_escuela_sabatica, UPPER(es.nombre) as escuela, es.id_unidadacad_univ, uu.id_univ_unidacad, u.nombre as facultad, u.abreviatura
FROM public.escuela_sabatica es, public.unidadacad_univ uu, public.univ_unidacad u
WHERE uu.id_unidadacad_univ = es.id_unidadacad_univ AND u.id_univ_unidacad = uu.id_univ_unidacad ORDER BY id_escuela_sabatica ASC");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}
function ModeloRepUser() {
    $stmt = Conexion::conectar()->prepare("SELECT id_usuario, login, foto, persona, rol FROM fn_report_usuario() ORDER BY login;");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}






