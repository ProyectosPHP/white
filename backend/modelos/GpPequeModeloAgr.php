<?php

require_once 'conexion.php';
$fecha = new DateTime();
$timestamp = $fecha->format('Y-m-d H:i:s');
function AgregarSemana($DatosModel) {
    $stmt = Conexion::conectar()->prepare("INSERT INTO public.semana(id_periodo, nombre, descripcion, fecha_ini, fecha_fin) VALUES (".$DatosModel["id_periodo"].", '".$DatosModel["nombre"]."', '".$DatosModel["descripcion"]."', '".$DatosModel["fecha_ini"]."','".$DatosModel["fecha_fin"]."')");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function AgregarGrupoPeriodo($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT public.fn_crud_grupo_periodo(null," . $DatosModel["id_periodo"] . "," . $DatosModel["id_iglesia"] . ",
    " . $DatosModel["id_escuela_sabatica"] . "," . $DatosModel["id_grupo_pequeno"] . "," . $DatosModel["id_carrera"] . ",'" . $DatosModel["ciclo"] . "',"
            . "'" . $DatosModel["lema"] . "','" . $DatosModel["canto"] . "','" . $DatosModel["versiculo"] . "','" . $DatosModel["blanco_baut"] . "','','1'");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function ModelAgregarModuloperfil($DatosModel) {
    $stmt = Conexion::conectar()->prepare("INSERT INTO public.rol_opcion( id_rol, id_modulo) VALUES (" . $DatosModel["id_rol"] . ", " . $DatosModel["id_modulo"] . ")");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function AgregarGrupo($DatosModel) {
    $stmt = Conexion::conectar()->prepare("INSERT INTO public.grupo_pequeno(id_iglesia, nombre, fecha_reg, estado) VALUES (" . $DatosModel["iglesias"] . ",'" . $DatosModel["nomgrupe"] . "',now(),'1')");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function ActualizarDatos($DatosModel) {
    $stmt = Conexion::conectar()->prepare("UPDATE public.grupo_periodo SET lema='" . $DatosModel["lema"] . "', canto='" . $DatosModel["canto"] . "', versiculo='" . $DatosModel["versiculo"] . "', blanco_baut=" . $DatosModel["blanco_baut"] . " WHERE id_grupo_periodo=" . $DatosModel["id_grupo_periodo"] . "");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function AgregaoGrupopersona($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT public.fn_reg_integrante(null," . $DatosModel["idgruper"] . "," . $DatosModel["idpersona"] . ",null,'1')");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function AgregaoGrupopequeno($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT id, alert, mensaje from fn_add_gp_gpperiodo(1,'" . $DatosModel["nombregrp"] . "'," . $DatosModel["periodo"] . ",null," . $DatosModel["carrera"] . "," . $DatosModel["lider"] . ")");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModeloCarrerass($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT id_pers, alert_pers, mensaje_pers FROM fn_crud_persona_universidad (null, null, '" . $DatosModel[""] . "','" . $DatosModel[""] . "', '" . $DatosModel[""] . "','" . $DatosModel[""] . "', 
" . $DatosModel[""] . ", '" . $DatosModel[""] . "'," . $DatosModel[""] . "," . $DatosModel[""] . ", null, null, 'yes', '', 'f', '', '', '', '1', '2')");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function ModelActualizarCargo($DatosModel) {
    $stmt = Conexion::conectar()->prepare("SELECT alert,mensaje FROM  public.fn_crud_cargo_integrante(" . $DatosModel["id_integrante_cargo"] . "," . $DatosModel["id_cargo"] . "," . $DatosModel["id_registro"] . ",'1','2')");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}
