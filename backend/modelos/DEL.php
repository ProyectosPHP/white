<?php

function MODELODELGrupoPeriodo($ID) {
    $stmt = Conexion::conectar()->prepare("DELETE FROM public.rol_opcion WHERE id_rol_opcion=$ID");
    if ($stmt->execute()) {
        return "success";
    } else {
        return "error";
    }

    $stmt->close();
}

function MODELODELInteg($Integrante_cargo,$id_registro) {
    $stmt = Conexion::conectar()->prepare("SELECT alert,mensaje FROM fn_delete_integrante($Integrante_cargo,$id_registro)");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}

function MODELOValidaEvaluar() {
    $stmt = Conexion::conectar()->prepare(" SELECT id, alert, mensaje FROM  fn_valida_dia_eval()");
    $stmt->execute();
    return $stmt->fetch();
    $stmt->close();
}
