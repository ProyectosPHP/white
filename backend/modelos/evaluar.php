<?php
require_once 'conexion.php';
function Modelocapturaridsemana() {
    try {
        $stmt = Conexion::conectar()->prepare("SELECT semana.id_semana,semana.descripcion, semana.fecha_ini, semana.fecha_fin 
FROM public.semana WHERE now() between  semana.fecha_ini AND semana.fecha_fin");
        $stmt->execute();
        return $stmt->fetchAll();
        $stmt->close();
    } catch (PDOException $e) {

        echo $e->getMessage();
    }
}

function Modeloevaluaritem($id) {
    $stmt = Conexion::conectar()->prepare("SELECT ec.id_evaluacion_config,ei.desripcion ,ec.id_evaluacion_item, ei.nombre, ec.estado
FROM public.evaluacion_config ec,public.evaluacion_item ei
WHERE ei.id_evaluacion_item = ec.id_evaluacion_item and ec.id_iglesia=$id order by desripcion asc");
    $stmt->execute();
    return $stmt->fetchAll();
    $stmt->close();
}

function Agregardatositemevaluar($DatosModel) {
    try {
        $stmt = Conexion::conectar()->prepare("SELECT public.fn_reg_evaluacion(" . $DatosModel["id_estudiante"] . ",'{" . $DatosModel["id_item"] . "}','{" . $DatosModel["valor"] . "}')");
        if ($stmt->execute()) {
            return "success";
        } else {
            return "error";
        }

        $stmt->close();
    } catch (PDOException $e) {

        echo $e->getMessage();
    }
}
