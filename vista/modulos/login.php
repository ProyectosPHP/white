<style>
    div#institucion{
        height: 60px;
        padding: 15px;
        margin-bottom: 5px;
    }
    img#i{
        height: 35px;
        width: 35px;
    } 
      @media screen and (max-width:1440px) and (min-width: 1024px) {
         div#institucion{
            
            text-align: justify;
          
        }
        h4#titulo{
            text-align: center;
           font-size: 20px;
        }

    }
    @media screen and (max-width: 1024px) and (min-width: 768px) {
         div#institucion{
            
            text-align: justify;
          
        }
        h4#titulo{
            text-align: center;
            font-size: 20px;
        }

    }
     @media screen and (max-width: 625px) and (min-width: 426px) {
         div#institucion{
            height:500px;
            
            text-align: justify;

        }
        h4#titulo{
            text-align: center;
            font-size: 20px;
        }
        img#i{
            height: 50px;
            width: 50px;
            margin-bottom: 5px;
        }
    }
    @media screen and (max-width: 768px) and (min-width: 425px) {
         div#institucion{
            height:150px;
            
            text-align: justify;

        }
        h4#titulo{
            text-align: center;
        }
        img#i{
            height: 50px;
            width: 50px;
            margin-bottom: 5px;
        }
    }
    @media screen and (max-width:425px) {
        div#institucion{
            height:170px;
            
            text-align: justify;
        }
        h4#titulo{
            font-size: 15px;
            text-align: center;
        }
        img#i{
            height: 70px;
            width: 70px;
        margin-bottom: 5px;    
        }
    }
</style>

<nav class="navbar navbar-toggleable-md navbar-dark scrolling-navbar fixed-top bg-primary">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapseEx2" aria-controls="collapseEx2" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#carousel-example-2">
            <img  src="vista/recursos/images/logo-upeu.png" class="img-responsive" height="50px;" width="50px;"> 
        </a>
        <div class="collapse navbar-collapse" id="collapseEx2">
            <ul class="navbar-nav mr-auto" id="ulnav" style="text-align: center;">
            </ul>
            <div class="nav-item pull-xs-right">
                <a href="index.php"  class="nav-link btn btn-secondary" id="inisec"><i class="fa fa-arrow-circle-o-left fa-lg" aria-hidden="true"></i>VOLVER</a>
            </div>
        </div>
    </div>
</nav>
<!--/.Navbar-->

<br>
<br>
<br>
<div class="divider-new">
    <h2 class="h2-responsive wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">ELIGA TU INSTITUCI&Oacute;N</h2>
</div>
<br>

<?php for ($i = 1; $i <= 5; $i++) { ?>
    <div id="institucion" class="container card  hoverable col-lg-10 col-xl-8" >

        <div class="row  center-on-small-only">
            <!--First column-->
            <div class="col-xs-12 col-sm-12 col-md-1  col-lg-1 col-xl-1">
                <div class="">
                    <div class="avatar">
                        <center> <img id="i" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(9).jpg" class="img-responsive "></center>
                    </div>
                </div>
            </div>
            <!--/First column-->
            <!--Second column-->
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center col-xl-7">
                <h4 id="titulo"><b>IGLESIA ADVENTISTA DEL SEPTIMO DIA-FILIAL TARAPOTO</b></h4>
            </div>
            <!--/Second column-->
            <!--tercero column-->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <!--Facebook-->
                <a class="icons-sm fb-ic view hm-zoom" href="#"><i class="fa fa-facebook"> </i></a>
                <!--Twitter-->
                <a class="icons-sm tw-ic" href="#"><i class="fa fa-twitter"> </i></a>
                <!--GitHub-->
                <a class="icons-sm git-ic" href="#"><i class="fa fa-github"> </i></a>
                <button class="icons-sm git-ic" type="submit" href="index.php?action=logeo"><i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></button>
            </div>
            <!--/tercero column-->
        </div>
        <!--/First row-->
    </div>
<?php } ?>
<script type="text/javascript">
    Waves.attach('.btn, .btn-floating', ['waves-light']);
    Waves.attach('.view .mask', ['waves-light']);
    Waves.attach('.waves-light', ['waves-light']);
    Waves.attach('.navbar li', ['waves-light']);
    Waves.init();
</script>