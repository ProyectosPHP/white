<style>
    .wow {
        visibility: visible;
    }

    html,
    body,
    header,
    .view {
        height: 100%;
    }
    /* Navigation*/

    .navbar {
        background-color: transparent;
    }

    .top-nav-collapse {
        background-color: #4285F4;
    }

    @media only screen and (max-width: 768px) {
        .navbar {
            background-color: #4285F4;
        }
    }
    /*Intro*/

    .view {
        background: url("vista/recursos/images/img25.jpg")no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    @media (max-width: 768px) {
        .full-bg-img,
        .view {
            height: auto;
            position: relative;
        }
    }

    .description {
        padding-top: 25%;
        padding-bottom: 3rem;
        color: #fff
    }

    @media (max-width: 992px) {
        .description {
            padding-top: 7rem;
            text-align: center;
        }
    }
</style>
<!--Navbar-->
<nav class="navbar navbar-toggleable-md navbar-dark fixed-top scrolling-navbar">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand waves-effect waves-light" href="" target="_blank"><img src="backend/vista/img/gp.png" class="img-responsive img-fluid" style="height: 30px;"></a>
        <div class="collapse navbar-collapse" id="navbarNav1">
            <ul class="navbar-nav mr-auto">

            </ul>
            <!--Navbar icons-->
            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item">
                    <a class="nav-link"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fa fa-instagram"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar-->

<!--Mask-->
<div class="view hm-black-strong">
    <div class="full-bg-img flex-center">
        <div class="container">
            <div class="row" id="home">

                <!--First column-->
                <div class="col-lg-6">
                    <div class="description">
                        <h2 class="h2-responsive wow fadeInLeft" data-wow-delay="0.3s">BIENVENIDOS! </h2>
                        <hr class="hr-dark wow fadeInLeft" data-wow-delay="0.3s">
                        <p class="wow wow fadeInLeft" data-wow-delay="0.3s">DISFUTRA DE UNA GRAN EXPERENCIA AL ADMINISTRAR TU GRUPO PEQUEÑO</p>
                        <br>
                        <a class="btn btn-outline-info waves-effect" data-wow-delay="0.3s">LEER MÁS...</a>
                    </div>
                </div>
                <!--/.First column-->

                <!--Second column-->
                <div class="col-lg-6">
                    <!--Form-->
                    <form id="frmvalida">
                        <div class="card wow fadeInRight" data-wow-delay="0.3s">
                            <div class="card-block">
                                <!--Header-->
                                <div class="text-center">
                                    <h3><i class="fa fa-user"></i>INICIA SESI&Oacute;N</h3>
                                    <a disabled="" class="btn-floating btn-fb btn-small"><i class="fa fa-facebook"></i></a>
                                    <a  disabled="" class="btn-floating btn-tw btn-small"><i class="fa fa-twitter"></i></a>
                                    <a  disabled="" class="btn-floating btn-gplus btn-small"><i class="fa fa-google-plus"></i></a>

                                    <hr>

                                </div>

                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                        <button type="button" id="form2" disabled="" class="form-control primary-color text-white" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">¿NO ESTAS EN EL PORTAL CORRECTO?</button>
                                        <div class="collapse" id="collapseExample">
                                            <select class="form-control" >
                                                <option>[ Seleccione portal correcto ]</option>
                                                <?php for ($o = 1; $o <= 5; $o++) { ?>
                                                    <option>BIENVENIDOS</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                                <!--Body-->
                                <div class="md-form">
                                    <i class="fa fa-envelope prefix"></i>
                                    <input type="text" id="correo"  name="correo" placeholder="INGRESAR CORREO" class="form-control">
                                    <label for="CORREO">CORREO</label>
                                </div>

                                <div class="md-form">
                                    <i class="fa fa-lock prefix"></i>
                                    <input type="password"  id="clave" name="clave" placeholder="INGRESA PASSWORD" class="form-control">
                                    <label for="form4">PASSWORD</label>
                                </div>

                                <div class="text-center">
                                    <!--<a class="btn btn-primary btn-lg" type="submit" href="backend" id="btnloguea"><i class="fa fa-sign-in" aria-hidden="true"></i> INGRESAR</a>-->
                                    <button class="btn btn-primary btn-lg" type="submit" href="backend" id="btnloguea"><i class="fa fa-sign-in" aria-hidden="true"></i> INGRESAR</button>
                                    <hr>
                                    <fieldset class="form-group">

                                        <a class="text-darken-1">HAS OLVIDADO TU CONTRASE&Ntilde;A?</a>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </form>
                    <!--/.Form-->
                </div>
                <!--/Second column-->
            </div>
        </div>
    </div>
</div>
<!--/.Mask-->
<script>
    new WOW().init();
</script>

