<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>WHITE UPEU</title>
        <link href="vista/recursos/images/logo-upeu.png" rel="shortcut icon" type="image/png">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="vista/css/font-awesome.min.css">
        <link href="vista/css/compiled.min_1.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <link href="vista/css/bootstrap.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="vista/css/mdb2.min.css" rel="stylesheet">
    <!-- Custom Style -->
    <link href="vista/css/style.min.css" rel="stylesheet">
    <link href="vista/css/overhang.min.css" rel="stylesheet">

    <!-- Prism -->
    <link href="vista/css/prism.min.css" rel="stylesheet">
        <!-- Template styles -->
        <!--<link href="css/carousel.css" rel="stylesheet">-->
        <script src="vista/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="backend/vista/funciones/validar.js"></script>
        <script>
            $(document).ready(function () {
                $('.carousel').carousel({
                    interval: 2000
                });
            });
        </script>

    </head>
    <body>
    <style>
        @media screen and (max-width: 768px) {
            #ulnav{
                background: black;
            }
        }
    </style>

    <?php
    $modulos = new enlaces();
    $modulos->enlacesController();
    ?>


    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="vista/js/jquery-3.1.1.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="vista/js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="vista/js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="vista/js/mdb.min.js"></script>
    <script type="text/javascript" src="vista/js/overhang.min.js"></script>


    <!-- Prism -->
    <script type="text/javascript" src="vista/js/prism.min.js"></script>
</body>

</html>
