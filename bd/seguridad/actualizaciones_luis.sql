﻿/*ALTUALIZAMOS LA FUNCIÓN DE ACCESO AL SISTEMA*/
CREATE OR REPLACE FUNCTION public.fn_acceso_sistema(
    IN _login character varying,
    IN _clave character varying)
  RETURNS TABLE(id_usuario integer, nombre text, usuario character varying, color character varying, foto character varying, id_rol integer, estado character varying) AS
$BODY$

DECLARE
	estado character varying;
BEGIN
	estado = (SELECT usu.estado FROM public.usuario usu WHERE usu.login=_login AND clave=_clave);

	IF estado = '1' THEN
		RETURN QUERY
			SELECT
				usu.id_usuario,
				pers.nombres || ' ' || pers.apepat || ' ' ||  pers.apemat,
				usu.login AS usuario,
				c.valor_hexa,
				usu.foto,
				usu.id_rol,
				usu.estado
			FROM
				public.usuario usu, 
				public.rol r, 
				public.color c,
				public.persona pers
			WHERE 
				usu.id_persona = pers.id_persona AND
				r.id_rol = usu.id_rol AND
				c.id_color = usu.id_color AND
				usu.login=_login AND clave=_clave;
	ELSE
		RETURN QUERY 
		SELECT -1, ''::text, ''::character varying, ''::character varying, ''::character varying, -1, '0'::character varying;
	END IF;						
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_acceso_sistema(character varying, character varying)
  OWNER TO postgres;
COMMENT ON FUNCTION public.fn_acceso_sistema(character varying, character varying) IS ' Script para llamar a la función: 
	SELECT id_usuario, nombre, usuario, color, foto, id_rol, estado FROM public.fn_acceso_sistema(character varying, character varying); ';

/*FUNCIÓN PARA APERTURAR GRUPOS PEQUEÑOS EXISTENTES EN UN NUEVO PERIODO*/
CREATE OR REPLACE FUNCTION public.fn_aperturar_grupo_periodo(
    IN _id_periodo_anterior integer,
    IN _id_periodo_nuevo integer)
  RETURNS TABLE(id integer, alert text, mensaje text) AS
$BODY$
DECLARE
   _id_retorno integer;
  _alert text; 
  _mensaje text;
   variable RECORD; ---- la variable RECORD funciona como una tabla de bd
   cursor_grupo_periodo CURSOR FOR 
			SELECT id_grupo_periodo, id_periodo, id_grupo_pequeno, id_carrera_gp, lema, 
				canto, versiculo, blanco_baut, fecha_reg_qp, estado, id_lider
			FROM public.grupo_periodo 
			WHERE id_periodo = _id_periodo_anterior AND id_grupo_pequeno 
			      NOT IN (SELECT id_grupo_pequeno FROM public.grupo_periodo WHERE id_periodo = _id_periodo_nuevo);
   
BEGIN
   _alert = 'success'; _mensaje = 'La apertura del nuevo grupo se realizó éxitosamente.';
   _id_retorno = _id_periodo_nuevo;
   OPEN cursor_grupo_periodo; --- habro el CURSOR para poder usarlo
   LOOP --- recorrer
      FETCH cursor_grupo_periodo INTO variable; --- traigo los datos del cursor y los inserto en la variable RECORD
      EXIT WHEN NOT FOUND; --- salir cuando no encuentre más datos
         INSERT INTO public.grupo_periodo(id_periodo, id_grupo_pequeno, id_carrera_gp, lema, canto, versiculo, blanco_baut, 
					  fecha_reg_qp, id_lider)
	 VALUES (_id_periodo_nuevo, variable.id_grupo_pequeno, variable.id_carrera_gp, variable.lema, variable.canto, 
		variable.versiculo, variable.blanco_baut, now(), variable.id_lider);
	---  RAISE NOTICE ' PROCESANDO  %', variable.id_grupo_pequeno;
   END LOOP; --- fin del recorrido
   RETURN QUERY       SELECT _id_retorno, _alert, _mensaje;	  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_aperturar_grupo_periodo(integer, integer)
  OWNER TO postgres;
COMMENT ON FUNCTION public.fn_aperturar_grupo_periodo(integer, integer) IS 'SELECT id, alert, mensaje FROM fn_aperturar_grupo_periodo( _id_periodo_anterior, _id_periodo_nuevo);';

/*APRETURAMOS GRUPOS PEQUEÑOS EN UN NUEVO PERIODO (PARA EL 2018-1)*/
SELECT public.fn_aperturar_grupo_periodo(1,2);
/*CAMBIAMOS EL NOMBRE DEL PRYMARY KEY AL NOMBRE DE LA TABLA DE DONDE PROVIENE*/
ALTER TABLE public.usuario RENAME COLUMN id_usuario TO id_persona;
/*ELIMINAMOS EL PRIMARY KEY*/
ALTER TABLE public.usuario DROP CONSTRAINT usuario_pkey;
/*AGREGAMOS EL CAMPO QUE SERÁ EL NUEVO PRYMARY KEY*/
ALTER TABLE public.usuario ADD COLUMN id_usuario serial;
/*HAECMOS QUE EL CAMPO SEA PRYMARY KEY*/
ALTER TABLE public.usuario ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);

/*CREAMOS UNA TABLA (usuario_rol) PARA PERMITIR AL USUARIO TENER MAS DE UN ROL*/
CREATE TABLE public.usuario_rol(  
  id_usuario_rol serial NOT NULL,
  id_usuario integer NOT NULL,
  id_rol integer NOT NULL,
  estado boolean NOT NULL DEFAULT true,
  CONSTRAINT usuario_rol_pkey PRIMARY KEY (id_usuario_rol),
  CONSTRAINT rol_opcion_id_usuario_fkey FOREIGN KEY (id_usuario)
      REFERENCES public.usuario (id_usuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT rol_opcion_id_rol_fkey FOREIGN KEY (id_rol)
      REFERENCES public.rol (id_rol) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

/*CREAMOS UN FUNCIÓN PARA LLENAR LA NUEVA TABLA CREADA (usuario_rol)*/
CREATE OR REPLACE FUNCTION llenar_usuario_rol() RETURNS void
AS
$BODY$
DECLARE
	var_usuario RECORD;
	cur_usuario CURSOR FOR SELECT id_usuario, id_rol FROM public.usuario ORDER BY id_usuario;
BEGIN
	OPEN cur_usuario;
	   LOOP
	    FETCH cur_usuario INTO var_usuario;
	    EXIT WHEN NOT FOUND;
		INSERT INTO public.usuario_rol(id_usuario, id_rol) VALUES (var_usuario.id_usuario, var_usuario.id_rol);
	    RAISE NOTICE ' %', var_usuario.id_usuario || '  '||var_usuario.id_rol ;
	   END LOOP;
	   RETURN;
END;
$BODY$
LANGUAGE plpgsql;
/*EJECUTAMOS PARA LLENAR LA NUEVA TABLA CREADA*/
SELECT public.llenar_usuario_rol();

/*LO QUE SIGUE ES PRUEBA*/
SELECT id_usuario, nombre, usuario, color, foto, id_rol, estado FROM public.fn_acceso_sistema('luis.lavado@upeu.edu.pe', 'luisrimam');

SELECT id_usuario_rol, id_usuario, id_rol, estado FROM public.usuario_rol WHERE id_usuario = 3;

INSERT INTO public.usuario_rol(id_usuario, id_rol) VALUES (3, 4);


