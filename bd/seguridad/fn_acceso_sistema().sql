﻿/*ALTUALIZAMOS LA FUNCIÓN DE ACCESO AL SISTEMA*/
CREATE OR REPLACE FUNCTION public.fn_acceso_sistema(
    IN _login character varying,
    IN _clave character varying)
  RETURNS TABLE(id_usuario integer, nombre text, usuario character varying, color character varying, foto character varying, id_rol integer, estado character varying) AS
$BODY$

DECLARE
	estado character varying;
BEGIN
	estado = (SELECT usu.estado FROM public.usuario usu WHERE usu.login=_login AND clave=_clave);

	IF estado = '1' THEN
		RETURN QUERY
			SELECT
				usu.id_usuario,				
				pers.nombres || ' ' || pers.apepat || ' ' ||  pers.apemat,
				usu.login AS usuario,
				c.valor_hexa,
				usu.foto,
				usu.id_rol,
				usu.estado
			FROM
				public.usuario usu, 
				public.rol r, 
				public.color c,
				public.persona pers
			WHERE 
				usu.id_persona = pers.id_persona AND
				r.id_rol = usu.id_rol AND
				c.id_color = usu.id_color AND
				usu.login=_login AND clave=_clave;
	ELSE
		RETURN QUERY 
		SELECT -1, ''::text, ''::character varying, ''::character varying, ''::character varying, -1, '0'::character varying;
	END IF;						
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_acceso_sistema(character varying, character varying)
  OWNER TO postgres;